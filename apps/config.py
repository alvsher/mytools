##################################################################################
#
# Alex Sherman
#    
###################################################################################

#
# Directories used in apps
#
proj_dir = '/home/alexs/projects/'
csv_gen = proj_dir + 'csv_gen/'
cur_dir = proj_dir + 'mytools/'
static_dir = cur_dir + 'static/'
log_dir = cur_dir + 'logs/'
apps_dir = cur_dir + 'apps/'

#
# Directories used in mytools.py and model.py
#
mytools_dir = cur_dir
mytools_apps = mytools_dir + 'apps/'
mytools_tools = mytools_dir + 'mytools_tools/'
mytools_static = mytools_dir + 'static/'
mytools_log = mytools_dir + 'logs/'
mytools_using = mytools_dir + 'using/'

