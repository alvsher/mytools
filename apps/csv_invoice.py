#!/usr/bin/env /usr/local/bin/python
##################################################################################
#
# ++++++++++  Input Files used at runtime  +++++++++++++++++++
#
# disks.csv        -----  Contains disk descriptions used in dmesg file
# hdd_tags.csv     -----  HDD models. Update with new HDD info when necessary  
# ssd_tags.csv     -----  SSD models. Update with new HDD info when necessary
#
# Alex Sherman
#
###################################################################################

from config import *

import sys
import os
import json
import psycopg2

import paramiko
import base64
import math

import threading
import time
from datetime import date, timedelta, datetime
from dateutil import parser

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

from email.parser import HeaderParser
import imaplib, email, re

from collections import Counter
import pandas

#
# Database connection data. No password!
#
db1_host = 'tools.alsher.com'
db1_db = 'mytools'
# db1_host = 'localhost'
# db1_db = 'alexdb'
db1_user = 'db12'
db1_con = None

fin_host = 'db.alsher.com'
fin_db = 'findb'
fin_user = 'fin1'
fin_con = None

fin_con_str = "psql -h db.alsher.com -U fin1 -d findb << EOF\n"
db1_con_str = "psql -h db1.alsher.com -U db12 -d db12 << EOF\n"
eof_str = ";\nEOF"

user = base64.b64decode('WVhOb1pYSnRZVzQ9')
passwd = base64.b64decode('VEhWaVlUQXdNREE9')

# CSV header
csv_header = "Customer PO Number,Customer Server Name or Type,Customer Asset Number,Serial Number,Invoice Date,Invoice," + \
        "Model Number,Form Factor,CPU Count,CPU Type,Cores per CPU,RAM,RAID Card," + \
        "HDD Count (A),HDD Type,HDD Size,HDD Count (B),HDD Type,HDD Size,SSD Count (A),SSD Size,SSD Count (B),SSD Size,SSD Count (C ),SSD Size," + \
        "PCIe NIC TYPE,IPMI MAC,Onboard NIC 1 MAC,Onboard NIC 2 MAC,Onboard NIC 3 MAC,Onboard NIC 4 MAC," + \
        "NIC 5 MAC,NIC 6 MAC,NIC 7 MAC,NIC 8 MAC,NIC 9 MAC, NIC 10 MAC, NIC 11 MAC, NIC 12 MAC"
#
# For Collecting, Logging, Testing, Debugging
#
system_id_list = []
ser_list = []
debug_list = []
ser_dir = {}
ser_sysinfo = {}
skipped = []
no_dmesg = []
disk_dict = {}
hdd_tag_dict = {}
ssd_tag_dict = {}
last_100_str = []

disk_info = csv_dir + 'disks.csv'
hdd_tags = csv_dir + 'hdd_tags.csv'
ssd_tags = csv_dir + 'ssd_tags.csv'

csv_result = static_dir

datetimetag = datetime.today().strftime("%Y%m%d-%H%M%S")
the_name = 'csv_order_' + datetimetag
logfilename = cur_dir + 'logs/' + the_name + '.txt'

logf = None
machine = ''
sysinfo = ''


##########################################################################
# Class to run 1 system in a thread
##########################################################################
class ship_one( threading.Thread ):

    ##########################################################################
    # Constructor
    ##########################################################################
    def __init__(self, ser, inv, ord, cur_email, inv_dict, ser_mdl_dict):
        self.ser = ser
        self.inv = inv
        self.ord = ord
        self.cur_email = cur_email
        self.inv_dict = inv_dict
        self.ser_mdl_dict = ser_mdl_dict
        threading.Thread.__init__(self)


    ##########################################################################
    # To run the tread starting from function do_sys_ser(self)
    ##########################################################################
    def run(self):
        self.do_sys_ser()


    ##########################################################################
    # This is to start the system serial processing
    ##########################################################################
    def do_sys_ser(self):
	ser = self.ser
	inv = self.inv
	ord = self.ord
	cur_email = self.cur_email
	inv_dict = self.inv_dict
	ser_mdl_dict = self.ser_mdl_dict

	sysinfo = self.get_sysinfo_mytools()
	if not sysinfo:
	      skipped.append(ser)
	      str1 = '\n               >>>>>>>>>>>>  sysinfo file not found in overseer DB for serial ' + ser + '\n'
	      print str1
	      cur_sys_dict = self.get_from_db()
	      mdl = cur_sys_dict['config']
	      if mdl[0:3] == 'cfg' and mdl[4] == '-':
		 cur_sys_dict['mdl'] = mdl[5:]
	      else:
		 cur_sys_dict['mdl'] = mdl
	      ser_sysinfo[ser] = cur_sys_dict
	      ser_sysinfo[ser].update(inv_dict)
	      cur_email[ser] = cur_sys_dict
	      cur_email[ser].update(inv_dict)
	      hdd = self.get_db_hdd_info()
	      self.insert_hdd_info(cur_email[ser], hdd)
	      ssd = self.get_db_ssd_info()
	      self.insert_ssd_info(cur_email[ser], ssd)
	      return

	str1 = '               +++++++ system serial %s, order %s, invoice %s\n' % (ser, ord, inv)
	print str1
	ser_sysinfo[ser] = self.parse_sysinfo(sysinfo)  # HDD and SSD info is taken from dmesg if applicable
	cur_email[ser] = ser_sysinfo[ser]

	cur_sys_dict = self.get_from_db()

	if 'hdd_cnt1' not in cur_email[ser] or cur_sys_dict['raid'] != 'N/A' and 'hdd_cnt1' in cur_email[ser] and 'hdd_cnt2' not in cur_email[ser]:
	   hdd = self.get_db_hdd_info()
	   self.insert_hdd_info(cur_email[ser], hdd)
	if 'ssd_cnt1' not in cur_email[ser]:
	   ssd = self.get_db_ssd_info()
	   self.insert_ssd_info(cur_email[ser], ssd)

	if 'cpu_num' not in cur_sys_dict or cur_sys_dict['cpu_num'] == 0:
	   cur_sys_dict['cpu_num'] = '1'        # CPU built into motherboard is not shown in DB 

	cor_per_cpu = int(cur_email[ser]['cpu_num']) / int(cur_sys_dict['cpu_num'])
	cur_sys_dict['cpu_cores'] = str(cor_per_cpu)
	dname = cur_email[ser]['test_dir']
	onboard, not_onboard = self.get_onboard_macs(dname)   # Extract macs from Burnin testing directory and select onboard
	cur_sys_dict['onboard_macs'] = onboard
	cur_sys_dict['other_macs'] = not_onboard
	cur_sys_dict['mdl'] = ser_mdl_dict.get(ser, 'N/A')

	ser_sysinfo[ser].update(cur_sys_dict)
	ser_sysinfo[ser].update(inv_dict)
	cur_email[ser].update(cur_sys_dict)
	cur_email[ser].update(inv_dict)


    #################################################################################
    # Get sysinfo from mytools database. It is the latest one 
    #################################################################################
    def get_sysinfo_mytools(self):
       sys_ser = self.ser
       sql1 = "select sysinfo from mv_sys_sysinfo where serial in ('%s')" % sys_ser
       cur = db1_con.cursor()
       cur.execute(sql1) 
       if cur.rowcount < 1:
          lst = []
       else:
          sysinfo = cur.fetchall()[0][0]
	  lst = [st.strip() for st in sysinfo.split('\n') if st.strip()][1:]

       cur.close()

       return lst


    #################################################################################
    # Extract macs from ifconfig with no ':' and separate onboard from others
    # returns both mac lists with no ':'
    #################################################################################
    def get_onboard_macs(self, dname):
       mac_lst = self.get_macs_from_db1()
       macs = [k.replace(':','') for k in mac_lst]

       # This one is onboard
       mac1 = os.path.basename(os.path.dirname(dname)).replace(':','')
       
       macs_onboard = filter(lambda m: abs(int(mac1, 16) - int(m, 16)) < 7, macs)
       macs_others = filter(lambda m: abs(int(mac1, 16) - int(m, 16)) > 6, macs)

       for i in range(0,len(macs_onboard)):
	   if unicode(str(macs_onboard[i]), "UTF-8").isdecimal() and str(macs_onboard[i])[0] == '0':
	      m = str(int(macs_onboard[i]))
	      macs_onboard[i] = (12 - len(m)) * 'O' + m

       for i in range(0,len(macs_others)):
	   if unicode(str(macs_others[i]), "UTF-8").isdecimal() and str(macs_others[i])[0] == '0':
	      m = str(int(macs_others[i]))
	      macs_others[i] = (12 - len(m)) * 'O' + m
       
       return macs_onboard, macs_others


    #################################################################################
    # DB mac set is taken
    #################################################################################
    def get_macs_from_db1(self):
        sql1 = "select mac from v_system_mac where system_serial = '%s' " % self.ser + \
               "and mac_name not like '%IPMI%'"
        db1_cur = db1_con.cursor()
        db1_cur.execute(sql1)
        lst  = [k[0] for k in db1_cur.fetchall()]
        db1_cur.close() 
        return lst


    ##########################################################################
    # Get all system parts rowset and create dictionary with extracted values
    ##########################################################################
    def get_from_db(self):
       ser = self.ser
       inv = self.inv
       ord = self.ord
       res = {}
       model = {}   # System part has model name (parts: cpu, motherboard, nic, raid, HDD, SSD,....)
       sql_parts = "select trim(both from a.serial) serial, a.model, a.type_id, c.name " + \
	      "from production_part a, production_system b, production_type c  " + \
	      "where a.system_id = b.id and a.type_id = c.id and a.rma = FALSE and b.system_serial = '%s'" % ser
       sql_config = "select a.config_name, b.asset_tag from production_configuration a, production_system b where b.system_serial = '%s' and b.config_name_id = a.id" % ser

       sql_mac = "select a.name, a.mac from production_mac a, production_system b where b.system_serial='%s' and a.system_id=b.id" % ser

       sql_po_quote = "select ponumber, notes from oe where ordnumber like '" + ord +"%" + "' and notes like '%Quote%' and ponumber <> ''"

       sql_ffactor = "select distinct substring(description from 0 for 120) from parts where partnumber like '%s'"
       sql_ffactor_end = " and (description like '% _U %' or description like '_U %')"

       db1_cur = db1_con.cursor()
       db1_cur.execute(sql_parts)
       rowset1 = db1_cur.fetchall()
       count_dict = Counter(elem[2] for elem in rowset1)
       for part in rowset1:     # Type ID is the key to the part model name. If several of the same type id, the last model is taken.
	   model[part[2]] = part[1].split(',')[0]

       res['cpu_num'] = count_dict[6]
       res['psu_num'] = count_dict[14]

       res['ram_model'] = model.get(1,'N/A')     # Memory model
       res['cpu_type'] = model.get(6,'N/A')      # CPU type
       res['motherboard'] = model.get(3,'N/A')   # Motherboard
       res['raid'] = model.get(4,'N/A')          # RAID card
       res['psu'] = model.get(14,'N/A')          # Power Supply
       res['nic'] = model.get(5,'N/A')           # PCI board

       db1_cur.execute(sql_config)
       rs_config = db1_cur.fetchone()
       res['config'] = rs_config[0]
       res['ct_asset'] = rs_config[1].strip() 
       
       db1_cur.execute(sql_mac)
       rs_mac = db1_cur.fetchall()
       nic_macs=[]
       for row in rs_mac:
	   if 'IPMI' in row[0]:
	      res['ipmi_mac'] = row[1].replace(':','')
	   else:
	      nic_macs.append(row[1].replace(':',''))
       res['nic_macs'] = nic_macs

       fin_cur = fin_con.cursor()
       fin_cur.execute(sql_po_quote)
       rowset2 = fin_cur.fetchone()
     
       # logf.write(fin_cur.query)
       # print rowset2
       q = ''
       po = ''
       if rowset2:
	  po = rowset2[0]
	  q = filter(lambda k: 'Quote' in k, rowset2[1].split('\n'))
       quote = ''
       if q: quote = re.sub(r'[:#]',' ', q[0].strip()).split(' ')[-1]
       res['quote'] = quote
       res['po_num'] = po
     
       # Let's exstract Form Factor from SQL-Ledger
       #
       res['ffactor'] = ''
       ff = filter(lambda k: len(k)==2 and k[1]=='U' and unicode(k[0]).isdecimal(), res['config'].split('-'))
       if ff:
	  res['ffactor'] = ff[0]
       else:
	  cfg_name = res['config'].strip(' +')
	  if cfg_name[0:3] == 'cfg' or cfg_name[0:3] == 'cgf':
	     cfg_name = cfg_name[5:].split(' ')[0]

	  if cfg_name[0:2] == 'XX':
	     cfg_name = cfg_name[2:]
	  
	  sql_cmd = sql_ffactor % ('%' + cfg_name + '%')
	  sql_cmd += sql_ffactor_end
	  fin_cur.execute(sql_cmd)
	  rs_ff = fin_cur.fetchone()
	  # logf.write(fin_cur.query)
	  if rs_ff:
	     ffactor = filter(lambda k: len(k)==2 and k[1]=='U' and k[0].isdigit(), rs_ff[0].split(' '))
	     if ffactor: res['ffactor'] = ffactor[0]

       db1_cur.close()
       fin_cur.close()

       return res
       

    ##########################################################################
    # Using DB connection ectract info for HDDs manufacturing models (tags)
    # and use them as keys to dictionary based on dtags.csv
    # Return list of tuples like [(2, 'SATA3', '250GB'), (10, 'SATA3', '2TB')]
    ##########################################################################
    def get_db_hdd_info(self):
       ser = self.ser
       sql_hdd = "select split_part(translate(a.model, '.;:', ',,,'), ',',1) from production_part a, production_system b " + \
		 "where a.system_id = b.id and type_id = 2 and a.rma = FALSE and b.system_serial = '%s'" 
      
       db1_cur = db1_con.cursor()
       db1_cur.execute(sql_hdd % ser)
       rowset1 = db1_cur.fetchall()
       rs = rowset1
       for i in range(0,len(rs)):
	   rs_i = list(rs[i])
	   rs[i] = tuple(rs_i)
	   if '-' in rs[i][0]:
	      pos = rs[i][0].index('-')
	      if pos > 5:
		 rsl = list(rs[i])
		 rsl[0] = rsl[0][0:pos]
		 rs[i] = tuple(rsl)

       db1_cur.close()	      
       count_dict = Counter(elem[0].strip().upper() for elem in rs)
       hdd_updates =  list(set(count_dict.keys()) - set(hdd_tag_dict.keys()))
       for k in hdd_updates:
	    str1 = ''
	    print '!!!! New Disk Model: ' + k
	    # logf.write('\n\n!!!! New Disk Model: ' + k + '\n!!!!!! UPDATE lookup file hdd_tags.csv with it asap.  !!!!!!\n')
	    if k[0:2] == 'ST':  # It is Seagate disk and it has only 2 letters inside (my observation)
	       c2_in_k = ''.join([c for c in k[2:] if c.isalpha()])
	       st_c2_lst = [re.sub('\d','',s[2:]) for s in hdd_tag_dict.keys() if s.startswith('ST')]
	       if c2_in_k not in st_c2_lst:
		  str1 = "!!!! This seagate HDD " + k + " with not existed pair of letters in it " + c2_in_k + " !!!!"
		  print str1
		  # logf.write('\n' + str1 + '\n')

	    send_new_disk_msg(k + '\n' + str1, ser)

       tup = ('','')
       tmp_lst =  sorted([(hdd_tag_dict.get(m,tup)[0], hdd_tag_dict.get(m,tup)[1], count_dict[m]) for m in count_dict])
       if not tmp_lst:
	  return []

       lst = [[tmp_lst[0][2],tmp_lst[0][0],tmp_lst[0][1]]]
       for k in tmp_lst[1:]:
	   if k[0] == lst[-1][1] and k[1] == lst[-1][2]:
	      lst[-1][0] += k[2]
	   else:
	      lst.append([k[2],k[0],k[1]])
       tup_lst = [tuple(k) for k in lst]  

       return tup_lst


    ##########################################################################
    # Using DB connection ectract info for SSDs manufacturing models (tags)
    # and use them as keys to dictionary based on dtags.csv
    # Return list of tuples like [(2, '250GB'), (10, '2TB')]
    ##########################################################################
    def get_db_ssd_info(self):
       ser = self.ser
       sql_ssd = "select split_part(translate(a.model, '.;:', ',,,'), ',',1) from production_part a, production_system b " + \
		 "where a.system_id = b.id and type_id = 22 and a.rma = FALSE and b.system_serial = '%s'" 
      
       db1_cur = db1_con.cursor()
       db1_cur.execute(sql_ssd % ser)
       rowset1 = db1_cur.fetchall()

       if not rowset1.count:
	  return []

       db1_cur.close()
       ssd_models = []
       for row in rowset1:
	  if 'ServerDOM' in row[0] or 'AVAGO' in row[0]: continue
	  r1 = list(row)
	  if "Samsung" not in r1[0] and "KingDian" not in r1[0] and "SuperMicro" not in r1[0]:
	     r1[0] = row[0].split(' ')[-1]
	     row = tuple(r1)
	  if '/' in row[0]:
	     k = row[0].split('/')
	     if k[0] in ssd_tag_dict:
		ssd_models.append(k[0])
	     else:
		ssd_models.append(k[1])
	  else:
	     ssd_models.append(row[0])

       cnt_mdls = Counter(ssd_models).most_common()
       mdls = [m[0] for m in cnt_mdls]
       ssd_updates =  list(set(mdls) - set(ssd_tag_dict.keys()))
       for k in ssd_updates:
	    print '!!!! New SSD Model: ' + k 
	    # logf.write('\n\n!!!! New SSD Model: ' + k + '\n!!!!!! UPDATE lookup file ssd_tags.csv with it asap.  !!!!!!\n')
	    send_new_disk_msg(k, ser)

       return [(m[1], ssd_tag_dict.get(m[0],'')) for m in cnt_mdls]


    ##########################################################################
    # HDD amount, type, size. Maximum 2 of them
    ##########################################################################
    def insert_hdd_info(self, dict, dd):
	hdd = sorted(dd)
	if len(hdd):
	   dict['hdd_cnt1'] = hdd[0][0]
	   dict['hdd_type1'] = hdd[0][1]
	   dict['hdd_size1'] = hdd[0][2]
	if len(hdd) > 1:
	   dict['hdd_cnt2'] = hdd[1][0]
	   dict['hdd_type2'] = hdd[1][1]
	   dict['hdd_size2'] = hdd[1][2]


    ##########################################################################
    # SSD amount, size. Maximum 3 pairs
    ##########################################################################
    def insert_ssd_info(self, dict, dd):
	ssd = sorted(dd)
	if len(ssd):
	   dict['ssd_cnt1'] = ssd[0][0]
	   dict['ssd_size1'] = ssd[0][1]
	if len(ssd) > 1:
	   dict['ssd_cnt2'] = ssd[1][0]
	   dict['ssd_size2'] = ssd[1][1]
	if len(ssd) > 2:
	   dict['ssd_cnt3'] = ssd[2][0]
	   dict['ssd_size3'] = ssd[2][1]


    ##########################################################################
    # Extract info from sysinfo file
    ##########################################################################
    def parse_sysinfo(self, sysinfo):
	ser = self.ser
	res = {}
	res['test_dir'] = sysinfo[1].split(' ')[-1]
	res['cpu_num'] = sysinfo[6].split(':')[1].strip()    # Correct number is in DB
	res['ram'] = str(int(int(sysinfo[9].split(':')[1].strip()[0:-2]) / 1024.0 + 0.5)) + 'GB'
	disks = sysinfo[10].split(':')[1].strip().split(' ')
	res['disk_num'] = len(disks)                   # We don't use this info
	cpu_model = sysinfo[5].split(':')[1].lstrip()
	ind = cpu_model.find('CPU') + 4
	res['cpu_type'] = cpu_model[ind:].split('@')[0].replace(' ','')

	hdd = self.get_db_hdd_info()
	ssd = self.get_db_ssd_info()
	self.insert_hdd_info(res, hdd)
	self.insert_ssd_info(res, ssd)
	
	return res



##################################################
# Error message printing in files and db1out
##################################################
def msg_out(msg):
    print msg
    logf.write(msg + '\n')


#############################################################
# Get from DDB DB by psql << EOF with SQL statement as input
#############################################################
def select_from_db1_no_con(sql1):
    cmd = db1_con_str + sql1 + eof_str
    return get_by_cmd(cmd).split('\n')[2:]


#############################################################
# Get connection to DB
#############################################################
def get_db_con(db, hst, usr):
    con = None
    try:
        con = psycopg2.connect(database=db, host=hst, user=usr)
    except psycopg2.DatabaseError, e:
        msg_out('\nError ' + str(e))
    return con


#############################################################
# Get ssh connection to prod1.alsher.com
#############################################################
def get_ssh_con(hostname):
    ssh_con = paramiko.SSHClient()
    ssh_con.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_con.connect(hostname, username=base64.b64decode(user), password=base64.b64decode(passwd))
    return ssh_con


#################################################################################
# Get Product name for systems from dmidecode taken from mytools
# In CSV file it named as Model Number. It returns dictionary {ser : mdl}
#################################################################################
def get_model_num(order, ser_tup):

    sql_mdl = "select serial, regexp_replace(split_part(split_part(dmidecode, 'Product Name: ',2),'Version: ',1),E'[\\n\\r\\u2028]+', ' ', 'g')" + \
                " model_number from mv_sys_dmidecode where serial in %s order by serial" % str(ser_tup).replace(',)',')')
 
    cur = db1_con.cursor()
    cur.execute(sql_mdl) 
    rs1 = cur.fetchall()
    cur.close()

    return {k[0]:k[1] for k in rs1}


##########################################################################
# Delete earlier files if amount of files more than specified amount
##########################################################################
def remove_extra_files(dname, amount):

    lst_files = [str(k).strip() for k in get_by_cmd('ls -t ' + dname).split('\n')]
    if len(lst_files) > amount:
       rm_cmd = 'rm -f ' + ' '.join([dname + '/' + k for k in lst_files[amount:]])
       # msg_out('rm_cmd: ' + rm_cmd)
       get_by_cmd(rm_cmd)


#############################################################
# Execute Unix cmd and return db1out 
#############################################################
def get_by_cmd(cmd):
    ftemp = os.popen(cmd)
    return ftemp.read().rstrip("\n")


######################################################################################
# To get info by invoice instead of parcing emails.
# Use DB viewer on asherman.alsher.com machine. 
######################################################################################
def get_invoice_info(inv_lst):
    info_dict = {}
    mail_str = []

    sql_head = "select customer,order_number,invoice,to_char(start_date,'YYYY-MM-DD'),to_char(end_date,'YYYY-MM-DD'),to_char(shipdate,'YYYY-MM-DD'),tracking,carrier from v_invoice_header where invoice = '%s'"    
    sql_sys  = "select system_serial from v_invoice_system where invoice = '%s'" 

    db_cur = db1_con.cursor()
    for inv in inv_lst:
       db_cur.execute(sql_head % inv)
       if db_cur.rowcount < 1:
           msg_out('\n  +++???+++  DB: There is no such invoice: ' + inv + '  +++???+++\n')
           continue
       rset = db_cur.fetchall()
       rs1 = rset[0]
       inv_dict = {'Company': rs1[0], 'Order': rs1[1], 'Invoice': rs1[2], 'Start Date': rs1[3], 'End Date': rs1[4], 'Shipment Date': rs1[5], 'Tracking Number': rs1[6], 'Carrier': str(rs1[7])}
       db_cur.execute(sql_sys % inv)
       rs2 = db_cur.fetchall()
       inv_ord = inv_dict['Invoice'] + '|' + inv_dict['Order']
       sys_lst = [k[0] for k in rs2] 
       info_dict[inv_ord] = [sys_lst, inv_dict]
       mail_str.append(str((inv_ord, sys_lst, inv_dict)))  # Stringize it 

    db_cur.close()

    return info_dict, mail_str


######################################################################################
# To get info by one order number.
######################################################################################
def get_order_info(order):
    info_dict = {}
    mail_str = []

    sql_sys = "select system_serial from v_order_cnfg_sys where order_number like '%" + order + "%'"

    db_cur = db1_con.cursor()
    db_cur.execute(sql_sys)
    rs2 = db_cur.fetchall()

    inv_dict = {'Company': order, 'Order': order, 'Invoice': order, 'Start Date': 'Jan. 01, 2000', 'End Date': 'Jan. 01, 2000', 'Shipment Date': 'Jan. 01, 2000', 'Tracking Number': '', 'Carrier': ''}
    inv_ord = inv_dict['Invoice'] + '|' + inv_dict['Order']
    sys_lst = [k[0] for k in rs2]
    info_dict[inv_ord] = [sys_lst, inv_dict]
    mail_str.append(str((inv_ord, sys_lst, inv_dict)))  # Stringize it

    db_cur.close()

    return info_dict, mail_str


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Send Disk Info For Update 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def send_new_disk_msg(disk, ser):
   sender = 'asherman@alsher.com'
   receivers = ['asherman@alsher.com']

   message = "From: Alex Sherman <asherman@alsher.com>\n" + \
             "To: To Alex Sherman <asherman@alsher.com>\n" + \
             "Subject: !!!! New Disk. Update Accordingly !!!!\n\n" + \
	     "  +++++ New Disk: '" + disk.strip() + "'\n" + \
             "  +++++ System: " + ser + "\n"
   try:
      smtpObj = smtplib.SMTP('mail.alsher.com')
      smtpObj.sendmail(sender, receivers, message)
   except:
      print "Error: unable to send email"


##########################################################################
# To read file with information for hdd models(tags) used in production
# The key is hdd model. The info manually placed in file from Internet.
# hdd_dct = {disk_model : SIZE, SAS/SATA)}
##########################################################################
def read_hdd_tags():
    fdisk = open(hdd_tags,'r')
    lst = fdisk.read().split('\n')[0:-1]    # Srip last empty line
    lists = [d.split(',') for d in lst]
    return {k[0]:(k[1],k[2]) for k in lists}


##########################################################################
# To read file with information for ssd models(tags) used in production
# The key is ssd model. The info manually placed in file from Internet.
# ssd_dct = {disk_model : SIZE}
##########################################################################
def read_ssd_tags():
    fdisk = open(ssd_tags,'r')
    lst = fdisk.read().split('\n')[0:-1]    # Srip last empty line
    lists = [d.split(',') for d in lst]
    return {k[0]:k[1] for k in lists}
 

##########################################################################
# To read file with lookup information for disks used in production
# Both SSD and HDD. KEY is description taken from dmesg file
# All lookup information taken from internet (Google) and put manually
# disks_dct = {disk_text : (HDD/SSD, SIZE, SAS/SATA)}
##########################################################################
def read_disk_info():
    fdisk = open(disk_info,'r')
    lst = fdisk.read().split('\n')[0:-1]
    lists = [d.split(',') for d in lst]
    return {k[0]:(k[1],k[2], k[3]) for k in lists}


##########################################################################
# Close files, connections, and send emails
##########################################################################
def close_all():
    # send_csv_by_email("asherman@alsher.com")
    # send_csv_by_email("shippingdb1@alsher.com")
    logf.close()
    db1_con.close
    fin_con.close()
    remove_extra_files(csv_dir + 'logs/', 160)


##########################################################################
# If there are new shipping emailes we start to fill empty csv file
##########################################################################
def header_to_csv():
    cmd = 'echo "' + csv_header + '" > ' + csv_result
    get_by_cmd(cmd)


##########################################################################
# Dictionary per each email (invoice) with system_serial as a key
##########################################################################
def csv_out(dict):
    csv_f = open(csv_result,'a')

    for ser in sorted(dict):
        dct = dict[ser]
        csv_f.write(dct['po_num'] + ',')
        csv_f.write(dct['config'] + ',')
        csv_f.write(dct['ct_asset'] + ',')
        csv_f.write(ser + ',')
        csv_f.write(dct['inv_date'] + ',')
        csv_f.write(dct['invoice'] + ',')
	csv_f.write(dct['mdl'] + ',')   # TBD: Model Number
        csv_f.write(dct['ffactor'] + ',')
        csv_f.write(str(dct.get('cpu_num','0')) + ',')
        csv_f.write(dct.get('cpu_type','') + ',')
        csv_f.write(dct.get('cpu_cores','') + ',')
        csv_f.write(dct.get('ram','') + ',')
        csv_f.write(dct.get('raid','') + ',')
        csv_f.write(str(dct.get('hdd_cnt1','')) + ',')
        csv_f.write(dct.get('hdd_type1','') + ',')
        csv_f.write(dct.get('hdd_size1','') + ',')
        csv_f.write(str(dct.get('hdd_cnt2','')) + ',')
        csv_f.write(dct.get('hdd_type2','') + ',')
        csv_f.write(dct.get('hdd_size2','') + ',')
        csv_f.write(str(dct.get('ssd_cnt1','')) + ',')
        csv_f.write(dct.get('ssd_size1','') + ',')
        csv_f.write(str(dct.get('ssd_cnt2','')) + ',')
        csv_f.write(dct.get('ssd_size2','') + ',')
        csv_f.write(str(dct.get('ssd_cnt3',' ')) + ',')
        csv_f.write(dct.get('ssd_size3','') + ',')
        csv_f.write(dct.get('nic','') + ',')
	csv_f.write(dct.get('ipmi_mac','') + ',')
	i = 0
	for mac in dct.get('onboard_macs',[]):
	    if i > 3: break
	    csv_f.write(mac + ',')
	    i += 1
	if i < 4:
	   for j in range(i,4):
               csv_f.write(' ,')
	       
	for mac in dct.get('other_macs',[]):  # Last columns. Does not matter where the end
	    csv_f.write(mac + ',')
        csv_f.write('\n')

    csv_f.write('\n\n')
    csv_f.close()	
	    
        
##########################################################################
# Iterate thru all new shipping emails. Extract info from DBs and Burnin.
# Populate data to CSV file and update email_str.txt file with new emails
##########################################################################
def iterate_mstrs(info_dict, new_strs):
    csv_header_printed = False
    for mstr in new_strs:
	k, sys_lst, dct = eval(mstr)
	inv_dict = {}
	[inv, ord] = k.split('|')
	inv_dict['invoice'] = inv
	inv_dict['order'] = ord
        ser_mdl_dict = get_model_num(ord, tuple(sys_lst))
        
        dt = info_dict[k][1]['Shipment Date']
        inv_dict['inv_date'] = parser.parse(dt).strftime('%m/%d/%y')
        str1 = '\n\n   ============  Order %s,  Invoice %s, Shipped %s  ============\n' % (ord,inv,dt)
	msg_out(str1)
	cur_email = {}
        thr = []
	for ser in info_dict[k][0]:
           thread1 = ship_one(ser, inv, ord, cur_email, inv_dict, ser_mdl_dict)
           thread1.setName(ser)
           thr.append(thread1)
        
        for t in thr:   # All threads store results in dictionary cur_email 
           t.start()
           # print t.getName(),'  +++ STARTED +++' 
        for t in thr:
           t.join()
           # print t.getName(),'  +++ ENDED +++' 
        if not cur_email.keys():
           continue
        if not csv_header_printed:
           header_to_csv()
           csv_header_printed = True
        # set_warranty(cur_email)
	csv_out(cur_email)
    

##########################################################################
#  main
#  The variable send_list contains emails to whom the results will be sent
##########################################################################
def main():
    global logf, static_dir, disk_dict, hdd_tag_dict, ssd_tag_dict, machine, \
           the_name, ser_sysinfo, db1_con, fin_con, logfilename, csv_result

    if len(sys.argv) < 3:
       print 'No Invoices.'
       exit(0)
    else:
       user = sys.argv[1]
       if not os.path.isdir(static_dir + user):
          print 'No directory in static for such user ', user
          exit(0)

       static_dir = static_dir + user + '/'
       invoice = sys.argv[2]
       the_name = user + '_' + the_name
       csv_result = static_dir + the_name + '_' + invoice + '.csv'
       logfilename = log_dir + the_name + '.log'
       
    logf = open(logfilename, 'w')
    db1_con = get_db_con(db1_db, db1_host, db1_user)
    fin_con = get_db_con(fin_db, fin_host, fin_user)  # Connection to SQL Ledger DB

    info_dict, new_strs = get_invoice_info([invoice])
    
    if not new_strs:
       msg_out("No such invoice in DB: " + str(invoice).strip('[]'))
       logf.close()
       db1_con.close()
       fin_con.close()
       exit(0)
       
    new_strs.sort()                      # Invoice number is at the beginning of each string

    hdd_tag_dict = read_hdd_tags()       # File hdd_tags.csv     --- {hdd_model : SIZE, SAS/SATA)}
    ssd_tag_dict = read_ssd_tags()       # File ssd_tags.csv     --- {ssd_model : SIZE}
    disk_dict = read_disk_info()         # File disks.csv        --- {disk_text : (HDD/SSD, SIZE, SAS/SATA)}

    #----------------------------------------------------------
    # disk_all_d = {disk_text : (HDD/SSD, SIZE, SAS/SATA)}
    #----------------------------------------------------------
    hdd_dct = {k : (disk_dict[k][1], disk_dict[k][2]) for k in disk_dict if disk_dict[k][0] == 'HDD'}
    ssd_dct = {k : disk_dict[k][1] for k in disk_dict if disk_dict[k][0] == 'SSD'}
    hdd_tag_dict.update(hdd_dct)
    ssd_tag_dict.update(ssd_dct)
 
    # header_to_csv()                     # Place header to csv file
    
    rc_csv = iterate_mstrs(info_dict, new_strs)  # Generates CSV file with path in csv_result

    close_all()                         # Closes all connections
    if rc_csv:
       pandas.DataFrame.from_csv(csv_result, index_col=False).dropna(axis=1,how='all').fillna('').to_html(csv_result.replace('.csv','.html'))

if __name__ == '__main__':
    main()
