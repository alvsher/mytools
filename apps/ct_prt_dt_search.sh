#!/bin/sh
#
# March 2017
#
# Alex Sherman
#
#################################################################################
u=$1
shift
ct=$1
dt1=$2
dt2=$3

y="Customer Parts: $ct from $dt1 till $dt2"
v=`python -c "print 100*'='"`
a="/tmp/$u_ct_sys_$$_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""ct_prt_"`date "+%H%M%S"`".html"

where="where lower(customer) like lower('%$ct%') and shipdate > '$dt1' and shipdate < '$dt2'"
q1="select distinct customer, order_number from v_system_sold_to_ct $where group by customer,order_number"
q2="select a.customer Customer, '$dt1' From_date, '$dt2' Till_date, count(*)::text Order_count, array_to_string(sort(array_agg(a.order_number)),', ') Orders from ($q1) a group by a.customer;"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:100%\">$v</div>';"
q9="select '<div align=center style=\"background-color:PowderBlue;color:Brown;width:100%;\"><h1>$y</h1></div>';"

r1=`psql -h tools.alsher.com -U std2 -d mytools -c "select distinct order_number from v_system_sold_to_ct $where order by order_number" | grep -v "row" | grep [1-9] | tr -d '\n' | sed 's/^ //g'`

g1="select system_serial,order_number ord_num,invoice,shipdate,config_name,wnty_expire,notes,smserialno,oemserial,asset_tag, macs from mv_order_ct_cfg_sys_all where system_serial = '"
g2="select part, model, rma, revision, part_count from v_sys_parts_ext_arr where system_serial = '"
g2_end="' order by system_serial, part, model;"
g3="select order_number, array_to_string(sort(serials), ', ') system_serials from v_order_systems_arr where order_number = '"

echo "\pset format wrapped" > $a
echo "\t" >> $a
echo $q9 >> $a
echo $q4 >> $a
echo "\t" >> $a
echo "\H" >> $a
echo "\t" >> $a
echo "\x" >> $a
echo $q2 >> $a
echo "\x" >> $a
echo "\t" >> $a
echo "\H" >> $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a
for o in $r1
do
   r2=`psql -h tools.alsher.com -U std2 -d mytools -c "select system_serial from v_order_cnfg_sys where order_number = '$o'" | grep A1- | sort | tr '\n' ' ' | sed 's/ $//g'`
   echo "\H" >> $a
   echo "$g3$o';" >>  $a
   echo "\H" >> $a
   for s in $r2
   do
      echo "\t" >> $a
      echo $q4 >> $a
      echo "\t" >> $a
      echo "\H" >> $a
      echo "$g1$s';" >> $a
      echo "$g2$s$g2_end" >> $a
      echo "\H" >> $a
   done
   echo "\t" >> $a
   echo $q4 >> $a
   echo "\t" >> $a
done

psql -h tools.alsher.com -U std2 -d mytools -f $a | grep -v -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>' -e '>&nbsp;<' | sed 's/border="1"/border="1" bordercolor=brown/g' > $t
rm -f $a

