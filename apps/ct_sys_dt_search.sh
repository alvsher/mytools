#!/bin/sh
#
# Usage ./ct_sys_dt_search.sh asherman Rahi 2017-01-01 2017-05-01
#
# May 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################
u=$1
shift
ct=$1
dt1=$2
dt2=$3

y="Customer Systems: $ct from $dt1 till $dt2"
v=`python -c "print 100*'='"`
a="/tmp/$u_ct_sys_$$_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""ct_sys_"`date "+%H%M%S"`".html"

where="where lower(customer) like lower('%$ct%') and shipdate > '$dt1' and shipdate < '$dt2'"
q1="select distinct customer, order_number from v_system_sold_to_ct $where group by customer,order_number"
q2="select a.customer, '$dt1' date1, '$dt2' date2, count(*)::text count, array_to_string(sort(array_agg(a.order_number)),', ') orders from ($q1) a group by a.customer;"
q3="select order_number, config_name, count(*) count, array_to_string(sort(array_agg(system_serial)),', ') systems from v_system_sold_to_ct $where group by customer,order_number,config_name order by order_number,config_name;"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:100%\">$v</div>';"
q9="select '<div align=center style=\"background-color:PowderBlue;color:Brown;width:100%;\"><h1>$y</h1></div>';"

echo $q1
echo $q2
echo $q3

echo "\pset format wrapped" > $a
echo "\t" >> $a
echo $q9 >> $a
echo $q4 >> $a
echo "\t" >> $a
echo "\H" >> $a
echo "\t" >> $a
echo "\x" >> $a
echo $q2 >> $a
echo "\x" >> $a
echo "\t" >> $a
echo "\H" >> $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a
echo "\H" >> $a
echo $q3 >> $a
echo "\H" >> $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a

psql -h tools.alsher.com -U std2 -d mytools -f $a | grep -v -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>' -e '>&nbsp;<' | sed 's/border="1"/border="1" bordercolor=brown/g' > $t
rm -f $a

