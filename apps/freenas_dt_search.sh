#!/bin/sh
#
# May 2017
#
# Alex Sherman
#
#################################################################################
u=$1
dt1=$2
dt2=$3

v=`python -c "print 100*'='"`
a="/tmp/$u_truenas_dt_$$_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""truenas_dt_search_"`date "+%H%M%S"`".html"

where="where shipdate > '$dt1' and shipdate < '$dt2' and lower(config_name) like '%freenas%'"

q1="select order_number,config_name,system_serial,customer,invoice,shipdate from v_nas_system_ship nas $where order by shipdate,order_number,invoice,system_serial;"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:100%\">$v</div>';"

echo "\pset format wrapped" > $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a
echo "\H" >> $a
echo $q1 >> $a
echo "\H" >> $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a
# echo "\H" >> $a
# echo $q3 >> $a
# echo "\H" >> $a

psql -h tools.alsher.com -U alex -d mytools -f $a | grep -v -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>' -e '>&nbsp;<' | sed 's/border="1"/border="1" bordercolor=brown/g' > $t
rm -f $a

