#!/bin/sh
#
# Usage ./get_asset_tags.sh A1-54821 A1-54822 A1-54823 A1-54824
#
# May 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################

for s in "$@"
do
    a=`psql -h alexstools.alexssystems.com -U std2 -d alexspal -c "select dmidecode from mv_sys_dmidecode where serial = '$s'" | grep 'Chassis Information' -A9 | grep 'Asset Tag' | sed 's/To .e .illed .y.*/Nothing/g' | tr -d ' ' | cut -d ':' -f2 | sed 's/^$/Nothing/g'`
    echo $s $a
done
