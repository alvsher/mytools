#!/bin/sh
#
# Usage ./get_disk_models.sh asherman A1-54821 A1-54822 A1-54823 A1-54824
#
# May 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################
u=$1
shift

a="/tmp/$u_disk_models"`date "+%H%M%S"`".txt"
rm -f $a
for s in "$@"
do
   echo $s >> $a
   psql -h alexstools.alexssystems.com -U std2 -d alexspal -c "select disk_info from sys_disk_info where serial = '$s'" | tr -d '+' | grep -i -e "Device Model:" -e "Product:" -e "Model Number = " -e "Serial number:" -e "SN =" -e ' Onln ' -e ' JBOD ' -e ' SSD ' -e ' HDD ' | grep -v "Logical " | tr -s ' ' | sed -e 's/SN = /%/g' -e 's/^S.*: /%/g' -e 's/ $//g' -e 's/^.*B /@/g' | sed 's/^/@/g' | tr '\n' ' ' | sed 's/ @%/%/g' | tr '@' '\n' | sed 's/^.*: //g' | grep -i "[a-z|\d]" >> $a

    echo "=====" >> $a
done
cat $a
rm -f $a
