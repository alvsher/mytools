#!/bin/sh
#
# Author Alex Sherman
# Software Architect
#
# It extracts all info from disk_info files 
# Parameters are SystemSerial_MACdir
#
# ./get_disks.sh A1-44364_d0:50:99:c0:44:56 A1-44638_0c:c4:7a:20:4d:b8
##############################################################################

a="/tmp/alex_disks"`date "+%H%M%S"`".txt"
rm -f $a
for v in "$@"
do
    dir=`echo $v | cut -d '_' -f2`
    # d=`ls -d /alexs/results/$dir/2*/disk_info | tail -1`
    d=`ls -d /alexs/results/0c:c4:7a:92:97:d2/2*/diskbuild | tail -1 | cut -c 1-43`
    echo $v >> $a
    cat $d/disk_info >> $a
    echo "===== SYSTEM LIST END =====" >> $a
done
cat $a
rm -f $a
