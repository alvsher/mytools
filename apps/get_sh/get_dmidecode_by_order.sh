#!/bin/sh
# 
# Author:  Alex Sherman
# Software Architect
#
# Parameters are directories containing dmesg file:
# ./get_dmidecode_by_order.sh <order number>
#######################################################################################################

psql overseer overseer -x -c "select a.serial,  convert_from(decode(files.data,'base64'),'SQL_ASCII') dmidecode from files, v_runid_last a where files.runid = a.runid and files.filename = 'dmidecode' and a.serial in (select distinct serial from sys where order_number = '$1')" | grep -i -v -e "Serial Number" -e "UUID" -e RECORD -e "# dmidecode" -e "Asset Tag:" | sed -e 's/          | //g' -e '/^$/d'


