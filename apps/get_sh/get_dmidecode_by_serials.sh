#!/bin/sh
#
# Author:  Alex Sherman
# Software Architect
#
# ./get_dmidecode_by_serials.sh A1-48971 A1-48972 A1-48973 A1-48974
#######################################################################################################

q=`python -c "print str('$*'.split()).strip('[]')"`

psql overseer overseer -x -c "select serial,  dmidecode from v_sys_dmidecode where serial in ($q)" | grep -i -v -e "Serial Number" -e "UUID" -e RECORD -e "# dmidecode" -e "Asset Tag:" | sed -e 's/          | //g' -e '/^$/d'

