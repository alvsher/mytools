#!/bin/sh
# 
# Author:  Alex Sherman
# Software Architect
#
# Parameters are directories containing dmesg file:
# ./get_fan_info.sh /alexs/results/00:25:90:f9:7a:78/20150608-1432 /alexs/results/00:25:90:f9:77:f8/20150608-1430
#######################################################################################################

for d in "$@"
do
    echo "=========="
    if [ -s $d/power ]
    then
       f=`grep FAN $d/power`
    else
       f=""
    fi
    grep DMI $d/sysinfo | cut -d ' ' -f4
    if [ ! -z "$f" ]
    then
       grep FAN $d/power | awk '{print $1,$2,$3,$4,$5,$6,$7}'
    else
       echo "** NO FAN INFO **"
    fi
done
