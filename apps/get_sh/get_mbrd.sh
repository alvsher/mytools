#!/bin/sh
# 
# Author:  Alex Sherman
# Software Architect
#
# Parameters are directories containing dmesg file:
# ./get_mbrd.sh /alexs/results/00:25:90:f9:7a:78/20150608-1432 /alexs/results/00:25:90:f9:77:f8/20150608-1430
#######################################################################################################

for d in "$@"
do
    grep DMI $d/sysinfo | cut -d ' ' -f4 | tr '\n' ','; grep -e 'BIOS Information' -e 'Base Board Information' -A3 $d/dmidecode | grep -e 'Version:' -e 'Product Name:' | cut -d ':' -f2 | sed 's/ //g' | tr '\n' ','; grep ipmi0 $d/dmesg | grep firmware | cut -d ' ' -f8 | sed 's/,//g';
done
