#!/bin/sh
# 
# Author:  Alex Sherman
# Software Architect
#
# Parameters are directories containing dmesg file:
# ./get_mraid.sh /alexs/results/0c:c4:7a:75:37:60/20151104-0324 /alexs/results/0c:c4:7a:56:94:24/20151027-1313
########################################################################################################

a="/tmp/alex_mraid"`date "+%H%M%S"`".txt"
rm -f $a
touch $a
for d in "$@"
do
  b=`grep -w MegaRAID $d/disk_info`
  if [ "$b" != ""  ]; then
    grep DMI $d/sysinfo | cut -d ' ' -f4 | tr '\n' ':';grep Controller -A4 $d/disk_info | grep -e 'Package' -e 'Version' -e 'Model' -e 'Serial' | sed -e 's/Model = /@/g' -e 's/ = /=/g' | cut -d '=' -f2 | tr '\n' ',' | sed -e 's/^@//g' -e 's/,$//g' -e 's/,@/@/g'; >> $a
  fi
done
cat $a | grep "A\d-\d\d\d\d\d"
rm -f $a
