#!/bin/sh
#
# March 2017
#
# Alex Sherman
#
#################################################################################
u=$1
shift

v=`python -c "print 100*'='"`
a="/tmp/invoice_systems_$$_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""invoice_systems_"`date "+%H%M%S"`".html"

q1="select customer,order_number,invoice,start_date,end_date,shipdate,tracking,carrier,array_to_string(system_serials,', ') system_serials from v_invoice where invoice = '$1';"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:50%\">$v</div>';"

echo "\pset format wrapped" > $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a
echo "\H" >> $a
echo "\x" >> $a
echo $q1 >> $a
echo "\x" >> $a
echo "\H" >> $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a

psql -h tools.alsher.com -U alex -d mytools -f $a | grep -v -i -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>' | sed 's/border="1"/border="1" bordercolor=brown/g' > $t

rm -f $a

