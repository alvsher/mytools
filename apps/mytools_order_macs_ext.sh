#!/bin/sh
#
# Usage ./mytools_order_macs_ext.sh asherman 18637
#
# Alex Sherman
# Software Architect
#
#################################################################################
u=$1
shift
v=`python -c "print 100*'='"`
a="/tmp/order_sys_parts_$$_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""order_sys_macs_"`date "+%H%M%S"`".html"

r=`psql -h tools.alsher.com -U std2 -d mytools -c "select system_serial from v_order_cnfg_sys where order_number = '$1'" | grep A1- | sort | tr '\n' ' ' | sed 's/ $//g'`

q1="select order_number,customer,config_name,system_serial,wnty_expire,notes,smserialno,oemserial,asset_tag,macs from v_order_ct_cfg_sys_all where system_serial = '"
# q1="select order_number, config_name, system_serial from v_order_cnfg_sys where system_serial = '"
# q3="select mac_name,mac from v_system_mac where system_serial ='"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:50%\">$v</div>';"
q5="select order_number, array_to_string(sort(serials), ', ') system_serials from v_order_systems_arr where order_number = '$1';"

echo "\pset format wrapped" > $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a
echo "\H" >> $a
# echo "\x" >> $a
echo $q5 >> $a
# echo "\x" >> $a
echo "\H" >> $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a

for s in $r
do
   echo "\H" >> $a
   echo $q1$s"';" >> $a
   echo "\t" >> $a
   # echo $q3$s"';" >> $a
   echo "\H" >> $a
   echo $q4 >> $a
   echo "\t" >> $a
done

psql -h tools.alsher.com -U std2 -d mytools -f $a | grep -v -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>' | sed 's/border="1"/border="1" bordercolor=green/g' > $t
rm -f $a

