#!/bin/sh
#
# Usage ./mytools_order_parts_ext.sh asherman 18637
#
# March 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################
u=$1
shift

y="System Parts of Order $1"
v=`python -c "print 100*'='"`
a="/tmp/order_sys_parts_$$_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""order_sys_parts_"`date "+%H%M%S"`".html"

# q1="select distinct order_number, config_name, system_serial from v_sys_parts_ext_arr where system_serial = '"
q1="select order_number,customer,config_name,system_serial,wnty_expire,notes,smserialno,oemserial,asset_tag,macs from mv_order_ct_cfg_sys_all where system_serial = '"
# q1="select order_number,customer,invoice,shipdate,config_name,system_serial,wnty_expire,notes,smserialno,oemserial,asset_tag,array_to_string(macs,', ') macs from v_order_ct_cfg_sys_all where system_serial = '"
q2="select part, model, rma, revision, part_count from v_sys_parts_ext_arr where system_serial = '"
q2_end="' order by system_serial, part, model;"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:100%\">$v</div>';"
q5="select order_number, array_to_string(sort(serials),', ') system_serials from v_order_systems_arr where order_number = '$1';"
q6="select '<div style=\"background-color:LightCyan;color:LightCyan;\">$v</div>';"
q9="select '<div align=center style=\"background-color:PowderBlue;color:Brown;width:100%;\"><h1>$y</h1></div>';"

echo "\pset format wrapped" > $a
echo "\t" >> $a
echo $q9 >> $a
echo $q4 >> $a
echo "\t" >> $a
echo "\H" >> $a
echo $q5 >> $a
echo "\H" >> $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a

r=`psql -h tools.alsher.com -U std2 -d mytools -c "select system_serial from v_order_cnfg_sys where order_number = '$1'" | grep A1- | sort | tr '\n' ' ' | sed 's/ $//g'`

for s in $r
do
   echo "\H" >> $a
   echo $q1$s"';" >> $a
   echo "\H" >> $a
   echo "\t" >> $a
   echo $q6 >> $a
   echo "\t" >> $a
   echo "\H" >> $a
   echo $q2$s$q2_end >> $a
   echo "\H" >> $a
   echo "\t" >> $a
   echo $q4 >> $a
   echo "\t" >> $a
done
psql -h tools.alsher.com -U std2 -d mytools -f $a | grep -v -i -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>' | sed 's/border="1"/border="1" bordercolor=blue/g' > $t
rm -f $a

