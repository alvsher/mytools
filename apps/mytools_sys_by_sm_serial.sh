#!/bin/sh
#
# Usage ./mytools_sys_by_sm_serial.sh asherman ECH170100030
#
#
# Alex Sherman
# Software Architect
#
#################################################################################
u=$1
sm=$2

y="Search System By SM Serial: $sm"
v=`python -c "print 100*'='"`
a="/tmp/$u_sys_by_smserialno_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""sys_by_smserialno_"`date "+%H%M%S"`".html"

r=`psql -h tools.alsher.com -U std2 -d mytools -tc "select distinct system_serial from mv_order_ct_cfg_sys_all where lower(smserialno) like lower('%$sm%')" | tr -d '\n' | sed 's/^ //g'`

# q2="select order_number,customer,invoice,shipdate,config_name,system_serial,wnty_expire,notes,smserialno,oemserial,asset_tag,macs from v_order_ct_cfg_sys_all where system_serial = '"
q2="select order_number,customer,invoice,shipdate,config_name,system_serial,wnty_expire,notes,smserialno,oemserial,asset_tag, macs from mv_order_ct_cfg_sys_all where system_serial = '"
q3="select part, model, manufacturer, serial, rma, revision, support_number from mv_sys_parts_ext where system_serial='"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:100%\">$v</div>';"
q9="select '<div align=center style=\"background-color:PowderBlue;color:Brown;width:100%;\"><h1>$y</h1></div>';"

echo "\pset format wrapped" > $a

if [ "${#sm}" -lt "4" ]; then
   v="Specified string $sm: String length should not be less than 4"
   echo "\t" >> $a
   echo "select '<div style=\"background-color:Moccasin;color:maroon;width:100%\">$v</div>';" >> $a
   echo "\t" >> $a
else
   echo "\t" >> $a
   echo $q9 >> $a
   echo $q4 >> $a
   echo "\t" >> $a

   for s in $r
   do
      echo "\H" >> $a
      echo "$q2$s';" >> $a
      echo "$q3$s' order by part, model, serial;" >> $a
      echo "\H" >> $a
      echo "\t" >> $a
      echo $q4 >> $a
      echo "\t" >> $a
   done
fi

psql -h tools.alsher.com -U std2 -d mytools -f $a | grep -v -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>'  | sed 's/border="1"/border="1" bordercolor=red/g' > $t
rm -f $a

