#!/bin/sh
#
# Usage ./mytools_sys_dmesg.sh alexs SSSSSSS
#
# March 2017
#
# Alex Sherman
#
#################################################################################

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$1/""dmesg_$2_"`date "+%H%M%S"`".txt"
s="select dmesg from mv_sys_dmesg where serial = '$2'"
psql -h tools.alsher.com -U std2 -d mytools -tc "$s" | sed -e 's/+$//g' -e 's/ *$//g' > $t

