#!/bin/sh
#
# Usage ./mytools_sys_parts_ext.sh asherman A1-53666 A1-53668 A1-53680
#
# March 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################
u=$1
shift
v=`python -c "print 100*'='"`
r=`echo "$@" | tr ',' '\n' | tr ' ' '\n' | sort | tr '\n' ' '`

q1="select order_number,customer,invoice,shipdate,config_name,system_serial,wnty_expire,notes,smserialno from mv_order_ct_cfg_sys_all where system_serial = '"
# q1="select order_number, config_name, system_serial from v_order_cnfg_sys where system_serial = '"
q2="select part, model, serial, rma, revision, support_number from v_system_parts where system_serial = '"
q2_end="' order by system_serial, part, model, serial;"
q3="select mac_name,mac from v_system_mac where system_serial ='"
q4="select '<div style=\"background-color:Moccasin;color:Moccasin;width:100%\">$v</div>';"

#
# $$ means shell process number
#
a="/tmp/sys_parts_$$_"`date "+%H%M%S"`".txt"

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$u/""sys_parts_"`date "+%H%M%S"`".html"

echo "" > $a
echo "\t" >> $a
echo $q4 >> $a
echo "\t" >> $a

for s in $r
do
   echo "\H" >> $a
   echo $q1$s"';" >> $a
   echo "\t" >> $a
   echo $q3$s"';" >> $a
   echo "\t" >> $a
   echo $q2$s$q2_end >> $a
   echo "\H" >> $a
   echo "\t" >> $a
   echo $q4 >> $a
   echo "\t" >> $a
done
psql -h tools.alsher.com -U std2 -d mytools -f $a | grep -v -e RECORD -e 'Expanded display' -e ' row' -e 'Showing only' -e 'Tuples only' -e 'Output format' -e '</p>'  | sed 's/border="1"/border="1" bordercolor=red/g' > $t
rm -f $a
