#!/bin/sh
#
# Usage ./mytools_sys_sysinfo.sh asherman A1-53666
#
# March 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################

x=`python -c "from config import mytools_static; print mytools_static.strip()"`
t="$x$1/""sysinfo_$2_"`date "+%H%M%S"`".txt"
s="select sysinfo from mv_sys_sysinfo where serial = '$2'"
psql -h tools.alsher.com -U std2 -d mytools -tc "$s" | sed -e 's/+$//g' -e 's/ *$//g' > $t

