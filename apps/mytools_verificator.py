#!/usr/local/bin/python
##################################################################################
#
# Alex Sherman
#
# To run the module:
# python validation.py
#
###################################################################################

from config import *

import sys
import os
import json
import psycopg2

import paramiko
import base64
import math

import threading
import re
import time
from datetime import date, timedelta, datetime
from dateutil import parser

from collections import Counter
from string import digits

import difflib
from difflib import *
diff = Differ()     # To use:  diff_lst = list(diff.compare(a,b))

#==============================================
# To work from browser
#==============================================
import web
from web import form
from subprocess import call

render = web.template.render('templates/', cache=False)

urls = ('/', 'index')
app = web.application(urls, globals())

myform = form.Form( 
    form.Textbox("Orders", size=40)) 
#=============================================

#
# Database connection data. No password!
#
db1_host = 'db1.alsher.com'
db1_db = 'db12'
db1_user = 'db12'
db1_con = None

sql_lg_host = 'db.alsher.com'
sql_lg_db = 'sql-ledger'
sql_lg_user = 'vpn_ro'
sql_lg_con = None

mytools_host = 'tools.alsher.com'
mytools_db = 'mytools'
mytools_user = 'db12'
mytools_con = None

ovsr_con_str = "psql overseer overseer << EOF\n"  # Connect from alexs.alsher.com (dbname and user)
sql_lg_con_str = "psql -h db.alsher.com -U vpn_ro -d sql-ledger << EOF\n"
db1_con_str = "psql -h db1.alsher.com -U db12 -d db12 << EOF\n"
eof_str = ";\nEOF"
html_out = "\\H\n\\o %s.html\n"       # system_serial or so instead of %s
html_off = "\n\\H"

user = base64.b64decode('WVhOb1pYSnRZVzQ9')
passwd = base64.b64decode('VEhWaVlUQXdNREE9')
ssh_db = None

#
# For Collecting, Logging, Testing, Debugging
#
ser_list = []
ser_dir = {}
ser_sysinfo = {}
skipped = []
no_dmesg = []
disk_dict = {}
hdd_tag_dict = {}
ssd_tag_dict = {}
thread_dct = {}

#
# Directories used
#
# proj_dir = '/usr/home/asherman/projects/'
# csv_dir = proj_dir + 'shipped2xlsx/'
# cur_dir = proj_dir + 'mytools/'
# static_dir = cur_dir + 'static/'
# apps_dir = cur_dir + 'apps/'

sysinfo = ''
disk_info = csv_dir + 'disks.csv'
hdd_tags = csv_dir + 'hdd_tags.csv'
ssd_tags = csv_dir + 'ssd_tags.csv'

datetimetag = datetime.today().strftime("%Y%m%d-%H%M%S")
the_name = 'validation_' + datetimetag
logfilename = cur_dir + 'logs/' + the_name + '.txt'

logf = None
# ord_logf_gl = None
machine = ''


##################################################
# Error message printing in files and db1out
##################################################
def msg_out(msg):
    print msg
    logf.write(msg + '\n')
    # if ord_logf_gl and not ord_logf_gl.closed:
    #    ord_logf_gl.write(msg + '\n')


##################################################
# Error message printing in files and db1out
##################################################
def msg_out2(msg, lf):
    msg_out(msg)
    lf.write(msg + '\n')
 

#############################################################
# Get from DB by psql << EOF with SQL statement as input
#############################################################
def select_from_ovsr_no_con(sql1):

    cmd = ovsr_con_str + sql1 + eof_str

    try:
        db1in, db1out, db1err = ssh_alexs.exec_command(cmd)
    except:
	print '   +++++++++ PROD1/Overseer Connection ERROR: \n'
	print "\n   ++++++ CMD: \n" + cmd
        msg_out("Failure: Connection to overseer with ssh to alexs\n")
	msg_out("   ++++++ CMD: \n" + cmd)
	return [], cmd

    return [str(k).strip('\n') for k in db1out.readlines()[2:-2]], cmd


#############################################################
# It works as this example:  Feb 12, 2015  ==> 2015-02-12
#############################################################
def get_db_date_format(dt):
    return str(parser.parse(dt))[0:10]


#############################################################
# Get connection to DB
#############################################################
def get_db_con(db, hst, usr):
    con = None
    try:
        con = psycopg2.connect(database=db, host=hst, user=usr)
    except psycopg2.DatabaseError, e:
        msg_out('\nError ' + str(e))
    return con


##################################################
# Get N days DAYTAG
##################################################
def get_n_days_daytag(n):
    dtags = []
    for i in range(0,n):
        dtags.append(str(date.today() - timedelta(days=i)).replace("-", ""))
    return dtags


#############################################################
# Get ssh connection to alexs.alsher.com
#############################################################
def get_ssh_con(hostname):
    ssh_con = paramiko.SSHClient()
    ssh_con.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_con.connect(hostname, username=base64.b64decode(user), password=base64.b64decode(passwd))
    return ssh_con


#############################################################
# Get ssh connection to alexs.alsher.com
#############################################################
def get_ssh_alexs_con(hostname):
    global ssh_alexs
    ssh_alexs = paramiko.SSHClient()
    ssh_alexs.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_alexs.connect(hostname, username=base64.b64decode(user), password=base64.b64decode(passwd))

    
###############################################################################
# Orders is a string with comma serparated order numbers
###############################################################################
def verify_orders(orders):

    if not orders:
       msg_out( 'No orders specified')
       return [], [] 
    
    msg_out( 'It was entered: ' + str(orders).strip('[]'))
    
    ord_str = str(tuple(orders)).replace(',)',')')
    sql_orders = "select order_number from production_order where order_number in " + str(ord_str)
    db1_cur = db1_con.cursor()
    db1_cur.execute(sql_orders)
    rowset = db1_cur.fetchall()
    ord_lst = [row[0] for row in rowset]
    str1 = 'Order list: ' + str(ord_lst)
    msg_out(sql_orders + '\n\n' + str1)
    
    skipped = [k for k in orders if k not in ord_lst]
    return ord_lst, skipped    


#############################################################
# Execute Unix cmd and return db1out 
#############################################################
def get_by_cmd(cmd):
    ftemp = os.popen(cmd)
    return ftemp.read().rstrip("\n")


#############################################################
# Execute Unix cmd on alexs and return db1out 
#############################################################
def get_by_cmd_from_alexs(cmd):
    
   try:
       db1in, db1out, db1err = ssh_alexs.exec_command(cmd)
   except:
       msg_out("ERROR: Connection Failure to PROD1")
       return [], cmd

   return [str(k).rstrip('\n') for k in db1out.readlines()], cmd


##########################################################################
# Delete earlier files if amount of files more than specified amount
##########################################################################
def remove_extra_files(dname, amount):

    lst_files = [str(k).strip() for k in get_by_cmd('ls -t ' + dname).split('\n')]
    if len(lst_files) > amount:
       rm_cmd = 'rm -f ' + ' '.join([dname + '/' + k for k in lst_files[amount:]])
       msg_out('rm_cmd: ' + rm_cmd)
       get_by_cmd(rm_cmd)


##########################################################################
# To read file with lookup information for disks used in production
# Both SSD and HDD. KEY is description taken from dmesg file
# All lookup information taken from internet (Google) and put manually
##########################################################################
def read_disk_info():
    disks = get_by_cmd('cat ' + csv_dir + 'disks.csv' )
    lists = [d.split(',') for d in disks.split('\n')]
    dct = {k[0]:(k[1],k[2], k[3]) for k in lists}
    disks = get_by_cmd('cat ' + csv_dir + 'hdd_tags.csv' )
    lists = [d.split(',') for d in disks.split('\n')]
    dct.update({k[0]:('HDD', k[1],k[2]) for k in lists})
    disks = get_by_cmd('cat ' + csv_dir + 'ssd_tags.csv' )
    lists = [d.split(',') for d in disks.split('\n')]
    dct.update({k[0]:('SSD',k[1],'') for k in lists})

    return dct
       

##########################################################################
# Close files, connections, and send emails
##########################################################################
def close_all():
    ssh_alexs.close()
    logf.close()
    db1_con.close()


##########################################################################
# Create 5 dicts listed in all_dicts
# 'ser_conf', 'conf_ser', 'ser_parts', 'parts_ser', 'conf_parts'
##########################################################################
def get_sys_serials(order):
    
    sql_parts = "select b.system_serial, d.config_name, a.type_id, a.model, c.name, a.revision, a.support_number " + \
                "from production_part a, production_system b, production_type c, production_configuration d, production_order e " + \
                "where a.system_id = b.id and a.type_id = c.id and a.rma = FALSE and b.config_name_id = d.id and d.order_number_id = e.id " + \
                "and b.config_name_id = d.id and e.order_number = '%s' order by b.system_serial, a.type_id, a.model" % order
    cur = db1_con.cursor()
    cur.execute(sql_parts)
    rowset = cur.fetchall()
    # print cur.rowcount
    rs = []
    for r in rowset:
	k = list(r)                     # k[0]-ser_num, k[1]-config, k[2]-type_id, k[3]-model, k[4]-name, k[5]-revision, k[6]-support_number
        if k[2] == 1:
           if '-' in k[3]:              # Memory. Truncate after dash
              k[3] = k[3].split('-')[0]
	   if k[6].strip():
              k[3] += ' / ' + k[6].strip().lower()
        elif k[2] in [3,4]:             # MotherBoard(3) or Raid(4) have revision value to compare
           k[3] += "/" + k[5]
        elif k[2] in [2,22]:
           k[3] += "," + k[5]

        rs.append(k)

    ser_conf_lst = list(set([(row[0], row[1]) for row in rs]))
    conf_ser_dict = {k[1] : [ser[0] for ser in ser_conf_lst if ser[1] == k[1]] for k in ser_conf_lst}
    ser_conf_dict = {k[0] : k[1] for k in ser_conf_lst}

    ser_parts_dict = {}

    for ser in ser_conf_dict:
	cont_lst = Counter([eval(str((k[2],k[3],k[4])).replace(' ','')) for k in rs if k[0] == ser]).most_common()
	cont_lst.append(((999,'Config',ser_conf_dict[ser]),0))
        ser_parts_dict[ser] = str(sorted(cont_lst))

    ser_asset_dct, asset_ser_dct = get_db1_asset_tags(order)
    parts_ser_dict = {val : [ser for ser in ser_parts_dict if ser_parts_dict[ser] == val] for val in ser_parts_dict.values()}

    conf_parts_dict = {conf : [parts for parts in parts_ser_dict if parts_ser_dict[parts][0] in conf_ser_dict[conf]] for conf in conf_ser_dict}

    all_dicts = {'ser_conf':ser_conf_dict, 'conf_ser':conf_ser_dict, 'ser_parts':ser_parts_dict, 'parts_ser':parts_ser_dict,
		 'conf_parts':conf_parts_dict, 'ser_asset':ser_asset_dct, 'asset_ser':asset_ser_dct}
    thread_dct['conf_ser'] =  conf_ser_dict
    return all_dicts


#################################################################################
# Get test directory from sysinfo on overseer with ssh to alexs.alsher.com
#################################################################################
def get_order_ser_dir_dict(ser_tup):
    print 'get_order_ser_dir_dict'

    sql_dir = "select serial, dir from mv_sys_dir_last where serial in %s;" % ser_tup
    ser_dir_dict = {str(x[0]) : str(x[1])  for x in db.query(sql_dir)}

    str1 = 'ser_tup: ' + ser_tup + '\n'
    logf.write('\n'+str1)
    logf.write('\n   +++++ Select statement To extract Directories of Test Results from sysinfo +++++\n\n%s\n\n' % sql_dir)

    if type(eval(ser_tup)) is str:    # When the only one system it will be string
       skipped = [k for k in [eval(ser_tup)] if k not in ser_dir_dict]
    else:
       skipped = [k for k in list(eval(ser_tup)) if k not in ser_dir_dict]

    if skipped:
       str1 = "\n   +++ " + str(len(skipped)) + " Systems not found on overseer: " + str(skipped).strip('[]') + "\n"
       print str1
       logf.write(str1)
    thread_dct['ser_dir'] = ser_dir_dict
    thread_dct['skipped'] = skipped
    return


#################################################################################
# Get test directory from sysinfo on overseer with ssh to alexs.alsher.com
#################################################################################
def get_order_ser_dir_end_dict(ser_tup):
    print 'get_order_ser_dir_end_dict'

    sql_dir = "select serial, dir from mv_sys_dir_last_end where serial in %s;" % ser_tup
    ser_dir_dict = {str(x[0]) : str(x[1])  for x in db.query(sql_dir)}

    ser_dir_end_dict = {str(k).split('|')[0].strip() : str(k).split('|')[1].strip() for k in info_end if 'A1-' in k}
    thread_dct['ser_dir_end'] = ser_dir_end_dict
    return


#################################################################################
# Extract information in parallel in different threads
#################################################################################
def thread_run (order, ser_tup):
    t1 = threading.Thread(name='get_order_ser_dir_dict',target=get_order_ser_dir_dict, args=(ser_tup,))
    t2 = threading.Thread(name='get_order_ser_dir_end_dict',target=get_order_ser_dir_end_dict, args=(ser_tup,))
    t3 = threading.Thread(name='get_dsk_dev_dict',target=get_dsk_dev_dict, args=(ser_tup,))
    t4 = threading.Thread(name='get_cpu_info',target=get_cpu_info, args=(order,ser_tup,))
    t5 = threading.Thread(name='get_mem_info',target=get_mem_info, args=(order,ser_tup,))
    t6 = threading.Thread(name='get_nics_info',target=get_nics_info, args=(order,ser_tup,))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t5.start()
    t6.start()

    t1.join()
    t2.join()
    t3.join()
    t4.join()
    t5.join()
    t6.join()

    dirs = thread_dct['ser_dir'].values()
    dirs_end = thread_dct['ser_dir_end'].values()
    t7  = threading.Thread(name='get_mraid_fw_pkg',target=get_mraid_fw_pkg, args=(order, dirs,))    
    t8  = threading.Thread(name='get_mboard_info',target=get_mboard_info, args=(order, dirs_end,))
    t9  = threading.Thread(name='get_ovsr_asset_tags',target=get_ovsr_asset_tags, args=(order, dirs,))    
    t10 = threading.Thread(name='get_disk_models',target=get_disk_models, args=(order, thread_dct,))
    t11 = threading.Thread(name='get_model_num',target=get_model_num, args=(order, thread_dct,))
    t12 = threading.Thread(name='get_fan_info',target=get_fan_info, args=(order, thread_dct,))
    t13 = threading.Thread(name='target=get_dmidecode',target=get_dmidecode, args=(ser_tup, thread_dct,))

    t7.start()
    t8.start()
    t9.start()
    t10.start()
    t11.start()
    t12.start()
    t13.start()

    t7.join()
    t8.join()
    t9.join()
    t10.join()
    t11.join()
    t12.join()
    t13.join()

    return 


#################################################################################
# Get Disk devices from sysinfo on overseer with ssh to alexs.alsher.com
#################################################################################
def get_dsk_dev_dict(ser_tup):
    print 'get_dsk_dev_dict'

    sql_dsk_dev = "select serial, regexp_replace(split_part(split_part(sysifo,'Disk devices:  ',2)," + \
          "'Incomplete run count:',1),E'[\\n\\r\\u2028]+', ' ', 'g') from mv_sys_sysinfo where serial in %s;" % ser_tup

    str1 = 'ser_tup: ' + ser_tup + '\n'
    logf.write('\n'+str1)
    logf.write('\n   +++++ Select statement To extract Disk devices from sysinfo +++++\n\n%s\n\n' % cmd)

    ser_ddev_dict = {str(x[0]).strip() : str(x[1]).strip() for x in db.query(sql_dsk_dev)}
    thread_dct['ser_ddev'] = ser_ddev_dict
    return
   

##########################################################################
# Collect from overseer DB as much as possible: RAID
##########################################################################
def get_mraid_fw_pkg(order,dirs):
    
    print 'get_mraid_fw_pkg'
    cmd = "/alexs/alexs/scripts/get_mraid.sh " + ' '.join(dirs)
    mraid_info, cmd1 = get_by_cmd_from_alexs(cmd)
    ser_mraid_d = {}
    for k in mraid_info:
	[ser,inf] = k.split(':',1)
        ser_mraid_d[ser] = []
        i = 0
        for chunk in inf.split('@'):
            controller = 'mrsas' + str(i) + ':'
            lst = chunk.split(',')
            if len(lst) == 4:
                ser_mraid_d[ser].append(controller + lst[0] + '/FW:' + lst[3] + '/PKG:' + lst[2])
                i += 1
    thread_dct['ser_raid'] = {ser: str(sorted(list(set(ser_mraid_d[ser])))).replace("', '",";").strip('[]').strip("'") for ser in ser_mraid_d}
    return


##########################################################################
# Collect from overseer DB as much as possible: CPU
##########################################################################
def get_cpu_info(order, ser_tup):
    print 'get_cpu_info'

    sql_cpu_mdl = "select serial, split_part(split_part(sysinfo,'CPU ',3),'@',1) from mv_sys_sysinfo where serial in %s" % ser_tup
    sql_cpu_cnt = "select serial, (length(dmidecode)-length(regexp_replace(dmidecode), 'Processor Information','','g')) where serial in %s" % ser_tup

    ser_cpu_dict = {str(x[0]).strip() : str(x[1]).strip() for x in db.query(sql_cpu_mdl)}
    ser_cnt_dict = {str(x[0]).strip() : str(x[1]).strip() for x in db.query(sql_cpu_cnt)}
    ser_cpu_dict = {ser_cpu_dict[ser].append(ser_cnt_dict[ser]) for ser in ser_cpu_dict if ser in ser_cnt_dict }

    thread_dct['ser_cpu'] = ser_cpu_dict
    return


##########################################################################
# Collect from overseer DB as much as possible: Memory
##########################################################################
def get_mem_info(order, ser_tup):
    print 'get_mem_info' 

    sql_mem_sz = "select serial, split_part(split_part(sysinfo),'Physical memory: ',2),'MB',1) where serial in %s" % ser_tup
    sql_mem_sp = "select serial, split_part(substring(dmidecode::text from '\tSpeed: \d* MHz'),' ',2) speed from mv_sys_dmidecode where serial in %s" % ser_tup

    ser_mem_sz_dct = {str(x[0]).strip() : [str(int(int(x[1].strip())/1024.0 + 0.5)) + "GB"] for x in db.query(sql_mem_sz)}
    ser_mem_sp_dct = {str(x[0]).strip() : str(x[1]).strip() for x in db.query(sql_mem_sp)}
    ser_mem_sz_dct = {ser_mem_sz_dct[ser].append(ser_mem_sp_dct[ser]) for ser in ser_mem_sz_dct if ser in ser_mem_sp_dct}

    thread_dct['ser_mem'] = ser_mem_dict
    return
 

##########################################################################
# Collect from overseer DB as much as possible: Motherboard
##########################################################################
def get_mboard_info(order,dirs):

    print 'get_mboard_info'
    cmd = "/alexs/alexs/scripts/get_mbrd.sh " + ' '.join(dirs)
    mbrd_info, cmd1 = get_by_cmd_from_alexs(cmd)
    ser_mbrd = {}
    for k in mbrd_info:
	lst = k.split(',')
        if len(lst) == 5:
	   ser_mbrd[lst[0]] = lst[2] + ',Rev:' + lst[3] + ', BIOS:' + lst[1] + ', IPMI:' + lst[4]
    thread_dct['ser_mbrd'] =  ser_mbrd
    return


##########################################################################
# Get Asset tags from DB for system_serials of one configuration
##########################################################################
def get_db1_asset_tags(order):
    sql_asset = "select c.system_serial, c.notes, c.asset_tag from production_order a, production_configuration b, production_system c " + \
		"where b.id = c.config_name_id and b.order_number_id = a.id and a.order_number = '%s'" % order
    
    db1_cur = db1_con.cursor()
    db1_cur.execute(sql_asset)
    rowset = db1_cur.fetchall()

    ser_asset_dct = {k[0] : k[2].strip() for k in rowset}
    ser_asset_dct1 = {k[0] : k[1].strip() for k in rowset}   # If asset_tag is still populated in notes 

    for k in ser_asset_dct:
        if ser_asset_dct[k] != '': continue
        if ser_asset_dct1[k] == '' or len(ser_asset_dct1[k]) > 20:
           ser_asset_dct[k] = 'Nothing'
        else:
           ser_asset_dct[k] = ser_asset_dct1[k]
    
    vals = list(set(ser_asset_dct.values()))
    asset_ser_dct = {a : [k for k in ser_asset_dct if ser_asset_dct[k] == a] for a in vals}

    return ser_asset_dct, asset_ser_dct


##########################################################################
# Get Asset tags from overseer dmidecode Chassis Information/Asset Tag
# Asset Tag: To Be Filled By O.E.M.
##########################################################################
def get_ovsr_asset_tags(order, dirs):
    print 'get_ovsr_asset_tags'
    cmd = "/alexs/alexs/scripts/get_asset_tags.sh " + ' '.join(dirs)
    asset_info,cmd1 =  get_by_cmd_from_alexs(cmd)
    ser_oasset_dct = {k.split(' ')[0] : k.split(' ')[1] for k in asset_info if k.split(' ')[0] != ''}
    thread_dct['ser_oasset'] = ser_oasset_dct
    return
 

#####################################################################################################
# To get model numbers for all systems from one order
# from overseer dmidecode. {ser : model_num}, {model : [ser]}, {conf_model_err: [model]}
#####################################################################################################
def get_model_num(order, all_dicts):
    print 'get_model_num'
    ser_tup = str(tuple(all_dicts['ser_conf'].keys())).replace(',)',')')

    sql_ = "select serial, regexp_replace(split_part(split_part(dmidecode),'Product Name: ',2),'Version: ',1),E'[\\n\\r\\u2028]+', ' ', 'g') model_num " + \
             "from mv_sys_dmidecode where serial in %s" % ser_tup

    ser_model_dct = {str(x[0]).strip() : str(x[1]).strip() for x in db.query(sql_)}
    model_ser_dct = {model : [ser for ser in ser_model_dct if ser_model_dct[ser] == model] for model in ser_model_dct.values()}
    conf_ser_dct = all_dicts['conf_ser']
    conf_model_dct = {conf : list(set([ ser_model_dct[ser] for ser in conf_ser_dct[conf] if ser in ser_model_dct])) for conf in conf_ser_dct}

    # Each configaration has one model number which is manually entered. Verify if it contains errors  
    # Find if 2 or more models within one configuration wich is an error
    conf_model_err_dct = {conf :  conf_model_dct[conf] for conf in conf_model_dct if len(conf_model_dct[conf]) > 1}
    thread_dct['model_ser'] = model_ser_dct
    thread_dct['conf_model_err'] = conf_model_err_dct
    return


#####################################################################################################
# To get FAN info for all systems from one order having that info in /alexs/results/*/*/power.
# Extract from file power. {ser : fan_info}, {fan_info : [ser]}, {conf_fan_info: [fan_info,...]}
#####################################################################################################
def get_fan_info(order, all_dicts):
    print 'get_fan_info'
    ser_dir_dct  = all_dicts['ser_dir_end']
    if not ser_dir_dct:
        thread_dct['fan_info_ser'] = {}
        thread_dct['conf_fan_info_err'] = {}
        return

    cmd = "/alexs/alexs/scripts/get_fan_info.sh "
    for ser in sorted(ser_dir_dct):
       cmd += ser_dir_dct[ser] + " "

    ln_lst, cmd1 = get_by_cmd_from_alexs(cmd)

    lst = '@'.join(ln_lst[1:]).split('@==========@')
    ser_fan_info_dct = {k.split('@',1)[0] : k.split('@',1)[1] for k in lst}
    fan_info_ser_dct = {fan_info : [ser for ser in ser_fan_info_dct if ser_fan_info_dct[ser] == fan_info] for fan_info in ser_fan_info_dct.values()}

    conf_ser_dct = all_dicts['conf_ser']
    conf_fan_info_dct = {conf : list(set([ ser_fan_info_dct[ser] for ser in conf_ser_dct[conf] if ser in ser_fan_info_dct])) for conf in conf_ser_dct}

    # Each configaration has one common FAN info for its systems. Verify if it contains errors  
    # Find if 2 or more different FAN infos within one configuration wich is an error
    conf_fan_info_err_dct = {conf :  conf_fan_info_dct[conf] for conf in conf_fan_info_dct if len(conf_fan_info_dct[conf]) > 1}
    thread_dct['fan_info_ser'] = fan_info_ser_dct
    thread_dct['conf_fan_info_err'] = conf_fan_info_err_dct
    return


#####################################################################################################
# Each configaration has one common dmidecode files for its systems. Verify if it contains errors
#####################################################################################################
def get_dmidecode(ser_tup, all_dicts):
    print 'get_dmidecode'
    str1 = ser_tup.replace('(', '[').replace(')',']')
    ser_str = ' '.join(eval(str1))
    cmd = "/alexs/alexs/scripts/get_dmidecode_by_serials.sh %s" % ser_str
    
    ln_lst, cmd1 = get_by_cmd_from_alexs(cmd)

    lst = '@'.join(ln_lst).split('@serial    | ')
    ser_dmidec_dct = {k.split('@',1)[0].replace('serial    | ','') : k.split('@',1)[1] for k in lst}
    dmidec_ser_dct = {dmidec : [ser for ser in ser_dmidec_dct if ser_dmidec_dct[ser] == dmidec] for dmidec in ser_dmidec_dct.values()}

    conf_ser_dct = all_dicts['conf_ser']
    conf_dmidec_dct = {conf : list(set([ ser_dmidec_dct[ser] for ser in conf_ser_dct[conf] if ser in ser_dmidec_dct])) for conf in conf_ser_dct}

    # Find if 2 or more different dmidecode files within one configuration wich is an error
    conf_dmidec_err_dct = {conf :  conf_dmidec_dct[conf] for conf in conf_dmidec_dct if len(conf_dmidec_dct[conf]) > 1}
    thread_dct['dmidec_ser'] = dmidec_ser_dct
    thread_dct['conf_dmidec_err'] = conf_dmidec_err_dct
    return


##########################################################################
# Collect from DB set of NICs
##########################################################################
def get_nics_info(order, ser_tup):
    print 'get_nics_info'
    sql_db1_nics= "select b.system_serial ser, a.name from production_mac a, production_system b where a.system_id = b.id and a.name not like 'IPMI%' " + \
		  "and b.system_serial in %s order by ser, name" % ser_tup
    
    db1_cur = db1_con.cursor()
    db1_cur.execute(sql_db1_nics)
    rowset = db1_cur.fetchall()
    thread_dct['ser_nics'] = {r[0] : [k[1] for k in rowset if k[0] == r[0]] for r in rowset}
    return


##########################################################################
# Extract disk devices string from sysinfo and transform to grep options
##########################################################################
def get_grep_disk_str(ser_ddev_dct):

    ser_dset = {ser : ser_ddev_dct[ser].split(' ') for ser in ser_ddev_dct}
    ser_opt = {}
    for ser in sorted(ser_dset):
	ser_opt[ser] = ''.join(['-e ' + s + '[0-9]*: ' for s in list(set([k.translate(None,digits) for k in ser_dset[ser]]))])

    return ser_opt


##########################################################################
# Collect disk info from system testing Burnin
##########################################################################
def get_disk_models(order, all_dicts):
   ser_dir_dct  = all_dicts['ser_dir']
   parms = [ser + '_' + ser_dir_dct[ser].split('/')[3] for ser in ser_dir_dct if '0000' not in ser and 'CLAB' not in ser]
   cmd = "/alexs/alexs/scripts/get_disk_models.sh " + ' '.join(parms)
   dmodels, cmd =  get_by_cmd_from_alexs(cmd)
   ser_dmodel = {}

   for k in dmodels:
       if k[0:3] == 'A1-':
	   ser = k.split('_')[0]
           lst = []
       elif '=====' in k:
           ser_dmodel[ser] = lst
       else:
           lst.append(k.split('%')[0])

   thread_dct['ser_dmodels'] = {ser : dict(Counter(ser_dmodel[ser])) for ser in ser_dmodel}
   return


##########################################################################
# Collect from overseer DB as much as possible: CPU, RAID, Memory, etc.
##########################################################################
def get_from_ovsr(order, **all_dicts):

    ser_tup = str(tuple(all_dicts['ser_conf'].keys())).replace(',)',')')
    thread_run(order,ser_tup)
    ser_dir_dct, ser_dir_end_dct, skipped = thread_dct['ser_dir'], thread_dct['ser_dir_end'], thread_dct['skipped']
    ser_cpu_dct, ser_mem_dct  = thread_dct['ser_cpu'], thread_dct['ser_mem']
    ser_ddev_dct, ser_raid_dct  = thread_dct['ser_ddev'], thread_dct['ser_raid']
    ser_mbrd_dct = thread_dct['ser_mbrd']
    ser_nics_dct = thread_dct['ser_nics']
    ser_oasset_dct, ser_dmodels_dct = thread_dct['ser_oasset'], thread_dct['ser_dmodels']
    
    model_ser_dct, conf_model_err_dct = thread_dct['model_ser'], thread_dct['conf_model_err']
    fan_info_ser_dct, conf_fan_info_err_dct = thread_dct['fan_info_ser'], thread_dct['conf_fan_info_err']
    dmidec_ser_dct, conf_dmidec_err_dct = thread_dct['dmidec_ser'], thread_dct['conf_dmidec_err']
    all_dicts.update(thread_dct)

    ser_conf_dct = all_dicts['ser_conf']
    ser_ovsr_d = {}
    for ser in ser_nics_dct:
	ser_ovsr_d[ser] = [
                            sorted(ser_nics_dct[ser]),
	                    ser_mem_dct.get(ser, 'N/A'), 
                            ser_cpu_dct.get(ser, 'N/A'), 
                            ser_mbrd_dct.get(ser, 'N/A'), 
                            ser_raid_dct.get(ser, 'N/A'),
                            ser_ddev_dct.get(ser, 'N/A')
                          ]

        dsk_dct = ser_dmodels_dct.get(ser,{})      # Default is no disks, i.e. {}
        for dsk in sorted(dsk_dct):
            dinfo = [dsk, 'Total: ' + str(dsk_dct[dsk])]
            dinfo.extend(list(disk_dict.get(dsk,('','',''))))
	    ser_ovsr_d[ser].append(str(dinfo))
        ser_ovsr_d[ser].append(ser_conf_dct[ser])
 
    ser_ovsr_dct = {ser : str(ser_ovsr_d[ser]) for ser in ser_ovsr_d}

    ovsr_ser_dct = {ovsr : [ser for ser in ser_ovsr_dct if ser_ovsr_dct[ser] == ovsr] for ovsr in ser_ovsr_dct.values()}
    conf_ser_dct = all_dicts['conf_ser']
    conf_ovsr_dct = {conf : [ovsr for ovsr in ovsr_ser_dct if ovsr_ser_dct[ovsr][0] in conf_ser_dct[conf]] for conf in conf_ser_dct}
     
    return {'ser_ovsr' : ser_ovsr_dct, 'ovsr_ser' : ovsr_ser_dct, 'conf_ovsr' : conf_ovsr_dct, 'ser_dir' : ser_dir_dct, \
            'ser_oasset' : ser_oasset_dct, 'model_ser' : model_ser_dct, 'conf_model_err' : conf_model_err_dct, \
            'fan_info_ser' : fan_info_ser_dct, 'conf_fan_info_err' : conf_fan_info_err_dct, 'dmidec_ser' : dmidec_ser_dct, 'conf_dmidec_err' : conf_dmidec_err_dct }


##########################################################################
# Compare Asset numbers on duplications and format 
##########################################################################
def db1_asset_compare(ord_repf, **all_dicts):

    ser_asset_d = all_dicts['ser_asset']
    asset_ser_d = all_dicts['asset_ser']

    ser_asset0_d = {ser : re.sub('\d','0',ser_asset_d[ser]) for ser in ser_asset_d}  # All 0 instead of digits
    vals = list(set(ser_asset0_d.values()))
    asset0_ser_d = {a0 : [k for k in ser_asset_d if re.sub('\d','0',ser_asset_d[k]) == a0] for a0 in vals}
    asset_dup_lst = [asset for asset in asset_ser_d if asset != 'Nothing' and len(asset_ser_d[asset]) > 1]
    
    if len(vals) > 1 or asset_dup_lst:
       msg_out2(45*'>' + 45*'<', ord_repf)
       msg_out2('>>>>>>>>>        D    I    S    C    R    E    P    A    N    C    E    S        <<<<<<<<<', ord_repf)
       msg_out2(45*'>' + 45*'<' + '\n', ord_repf)
    elif len(vals) == 1 and vals[0] != 'Nothing' and not asset_dup_lst:
       msg_out2(45*'>' + 45*'<', ord_repf)
       msg_out2('>>>>>>>>>         D_B :       A_S_S_E_T    T_A_G_S    A_R_E    O_K !!!         <<<<<<<<<', ord_repf)
       msg_out2(45*'>' + 45*'<' + '\n', ord_repf)

    if len(vals) > 1:
       msg_out2('   +++++++++  Asset Tags in DB with   D I F F E R E N T   M A S K  +++++++++', ord_repf)
       msg_out2('-'*90, ord_repf)
       for a0 in asset0_ser_d:
	   msg_out2(str(len(asset0_ser_d[a0])) + ' Systems: ' + str(asset0_ser_d[a0]).strip('[]').replace("'",""), ord_repf)
           msg_out2('Asset Number: ' + a0.replace('0','#') + '\n', ord_repf)
       msg_out2(45*'>' + 45*'<' + '\n', ord_repf)

    print asset_dup_lst
    if asset_dup_lst:
       msg_out2('   +++++++++       Asset Tags in STD with   D U P L I C A T I O N S       +++++++++', ord_repf)
       msg_out2('-'*90, ord_repf)
       for dup in asset_dup_lst:
           msg_out2(str(len(asset_ser_d[dup])) + ' Systems: ' + str(asset_ser_d[dup]).strip('[]').replace("'",""), ord_repf)
           msg_out2('Same Asset Number:   ' + dup, ord_repf)
       msg_out2(45*'>' + 45*'<' + '\n', ord_repf)


##########################################################################
# Print collected info for debugging
##########################################################################
def print_dict(**all_dicts):
    msg_out('============== ser_conf_dict ===============\n')
    ser_conf_dict = all_dicts['ser_conf']
    for k in sorted(ser_conf_dict):
       msg_out(k + ' <--> ' + ser_conf_dict[k])
    msg_out('')

    msg_out('============== conf_ser_dict ===============\n')
    conf_ser_dict = all_dicts['conf_ser']
    for k in sorted(conf_ser_dict):
       msg_out(k + '\n' + str(sorted(conf_ser_dict[k])) + '\n')

    msg_out('============== ser_parts_dict ===============\n')
    ser_parts_dict = all_dicts['ser_parts']
    for k in sorted(ser_parts_dict):
       msg_out(k + ' <--> ' + ser_parts_dict[k] + '\n')

    msg_out('============== parts_ser_dict ===============\n')
    parts_ser_dict = all_dicts['parts_ser']
    for k in sorted(parts_ser_dict):
       msg_out(str(sorted(parts_ser_dict[k])) + '\n' + k.replace('), (',')\n (') + '\n')

    msg_out('============== conf_parts_dict ===============\n')
    conf_parts_dict = all_dicts['conf_parts']
    parts_ser_dict = all_dicts['parts_ser']
    for conf in sorted(conf_parts_dict):
	msg_out('  ++++ Configuration: ' + conf + '\n')
	for k in conf_parts_dict[conf]:
	    str1 = 'System: '
	    if 'MotherBoard' not in str(k):
               str1 = 'JBOD: '
            msg_out(str1 + str(sorted(parts_ser_dict[k])) + '\n' + k.replace('), (',')\n (') + '\n\n')


##########################################################################
# Print Overseer collected info for debugging
##########################################################################
def print_ovsr_dict(**all_dicts):

    msg_out('\n============== ser_dir_dict ===============\n')
    ser_dir_dict = all_dicts['ser_dir']
    msg_out('\n                 +++ System Serial <--> directories dictionary +++\n')
    for k in sorted(ser_dir_dict):
       msg_out(k + ' <--> ' + str(ser_dir_dict[k]))

    ser_conf_dict = all_dicts['ser_conf']
    ser_not_on_ovsr = [k for k in ser_conf_dict.keys() if k not in ser_dir_dict]
    if ser_not_on_ovsr:
       msg_out('\n============== Systems Not Found On Overseer ===============\n')
       msg_out(' ' + str(len(ser_not_on_ovsr)) + ' Systems not on overseer: ' + str(sorted(ser_not_on_ovsr)).strip('[]') + '\n')

    msg_out('\n============== ser_ovsr_dict ===============\n')
    ser_ovsr_dict = all_dicts['ser_ovsr']
    msg_out('\n                 +++ NICs,   Memory,   CPU,  RAID, M_Board +++\n')
    for k in sorted(ser_ovsr_dict):
       msg_out(k + ' <--> ' + ser_ovsr_dict[k])

    msg_out('\n============== ovsr_ser_dict ===============\n')
    ovsr_ser_dict = all_dicts['ovsr_ser']
    msg_out('\n                 +++ Overseer info <--> serials dictionary +++\n')
    for k in sorted(ovsr_ser_dict):
       msg_out(k + '\n' + str(sorted(ovsr_ser_dict[k])))
    
    msg_out('\n============== conf_ovsr_dict ===============\n')
    conf_ovsr_dict = all_dicts['conf_ovsr']
    msg_out('\n                 +++ Configuration <--> Overseer info dictionary +++\n')
    for conf in sorted(conf_ovsr_dict):
       msg_out('  ++++ Configuration: ' + conf + '\n')
       for k in conf_ovsr_dict[conf]:
	   msg_out(k + '\n' + str(len(ovsr_ser_dict[k])) + " systems: " + str(sorted(ovsr_ser_dict[k])) + '\n')
    

##########################################################################
# Formated print for set of parts
##########################################################################
def print_parts(parts, max_sys_parts, ord_repf):

    head = ['-'*100, " type |" + "Model".center(55) + "|" + "Name".center(25) + "| amount", '-'*100]
    lst1 = list(head)
    for k in eval(parts):
       lst1.append(" " + str(k[0][0]).rjust(4) + " | " + k[0][1].ljust(54) + "| " + k[0][2].ljust(24) + "| " + str(k[1]).rjust(4))

    lst2 = list(head)
    for k in eval(max_sys_parts):
       lst2.append(" " + str(k[0][0]).rjust(4) + " | " + k[0][1].ljust(54) + "| " + k[0][2].ljust(24) + "| " + str(k[1]).rjust(4))

    diff_lst = list(diff.compare(lst1,lst2))

    for k in diff_lst:
	if '999' in k and 'Config' in k: continue
	msg_out2(k, ord_repf)


##########################################################################
# Create list of lines to compare and print
##########################################################################
def fill_ovsr_list(ovsr):
   k = eval(ovsr)
   lst = [ " ovsr | " + "NICs".ljust(14) + " | " + str(k[0]).ljust(59),
         " ovsr | " + "Memory".ljust(14) + " | " + str(k[1]).ljust(59),
         " ovsr | " + "CPU".ljust(14) + " | " + str(k[2]).ljust(59),
         " ovsr | " + "MotherBoard".ljust(14) + " | " + str(k[3]).ljust(59),
         " ovsr | " + "RAID".ljust(14) + " | " + str(k[4]).strip('[]').replace('(','').replace(')','').ljust(59),
         " ovsr | " + "Disk devices".ljust(14) + " | " + str(k[5]).ljust(59)]
   for i in range(6,len(k)-1):      # Exclude configuration, it is the last one
       lst.append(" ovsr | " + "Disk".ljust(14) + " | " + str(k[i]).ljust(59))

   if not k[4]:
      lst.pop(4)
 
   return lst

 
##########################################################################
# Formated print for Overseer extracted info with compare
##########################################################################
def print_ovsr(ovsr, max_sys_ovsr, ord_repf):

    head = ['-'*80, " type |" + "Name".center(15) + " | " + "Model/Values".center(60), '-'*80]
    lst1 = list(head)                 # To get it as deep copy we must use list()
    lst1.extend(fill_ovsr_list(ovsr))

    lst2 = list(head)                 # To get it as deep copy we must use list()
    lst2.extend(fill_ovsr_list(max_sys_ovsr))

    diff_lst = list(diff.compare(lst1,lst2))

    for k in diff_lst:
	msg_out2(k, ord_repf)
    
 
##########################################################################
# Print amount of systems distributed in set of configurations
##########################################################################
def print_db1_conf_distr( ord_repf, **all_dicts):
    
    conf_parts_d = all_dicts['conf_parts']
    parts_ser_dict = all_dicts['parts_ser']
    conf_ser_dict =  all_dicts['conf_ser']

    report = ['-'*90, " Configuration".center(40) + "|" + "Total systems".center(15) + "|" + "Valid Systems".center(15) + "|   Invalid", '-'*90]

    for conf in conf_parts_d:
	max_sys_parts = conf_parts_d[conf][0]
        for parts in conf_parts_d[conf]:
            if len(parts_ser_dict[parts]) > len(parts_ser_dict[max_sys_parts]):
               max_sys_parts = parts
	
	conf_ser_lst = conf_ser_dict[conf]
	max_sys_lst = parts_ser_dict[max_sys_parts]
	total = len(conf_ser_lst)
	most = len(max_sys_lst)
        diffs = total - most
	report.append(" " + conf.ljust(38) + " | " + str(total).rjust(13) + " | " + str(most).rjust(13) + " | " + str(diffs).rjust(13))

    msg_out2("\n        DB Verification Report for Order: " + os.path.basename(ord_repf.name).replace('.txt',''), ord_repf)
    for line in report:
        msg_out2(line, ord_repf)
	
    msg_out2('-'*90 + '\n\n', ord_repf)

    db1_asset_compare(ord_repf, **all_dicts)


##########################################################################
# Print amount of systems distributed in set of configurations
##########################################################################
def print_ovsr_conf_distr( ord_repf, **all_dicts):
    
    conf_ovsr_d = all_dicts['conf_ovsr']
    ovsr_ser_dict = all_dicts['ovsr_ser']
    conf_ser_dict = all_dicts['conf_ser']

    ser_dir_dict = all_dicts['ser_dir']
    ser_conf_dict = all_dicts['ser_conf']

    ser_asset_d = all_dicts['ser_asset']
    ser_oasset_d = all_dicts['ser_oasset']
    
    ser_not_on_ovsr = [k for k in ser_conf_dict.keys() if k not in ser_dir_dict]
    if ser_not_on_ovsr:
       msg_out2('\n============== Systems Not Found On Overseer ===============\n', ord_repf)
       msg_out2(' ' + str(len(ser_not_on_ovsr)) + ' Systems not on overseer: ' + str(sorted(ser_not_on_ovsr)).strip('[]'), ord_repf)
       msg_out2('-'*90 + '\n', ord_repf)

    ################################
    #####     Asset Numbers    #####
    ################################
    ser_asset0_d = {ser : re.sub('\d','0',ser_asset_d[ser]) for ser in ser_asset_d}  # All 0 instead of digits
    vals = list(set(ser_asset0_d.values()))

    lst_diff = [ser for ser in ser_oasset_d if ser_oasset_d[ser] != ser_asset_d[ser].split(' ')[-1] and (ser_oasset_d[ser] != 'Defaultstring' or ser_asset_d[ser] != 'Nothing')]
    if lst_diff:
       msg_out2(45*'>' + 45*'<', ord_repf)
       msg_out2('>>>>>>>>>        D    I    S    C    R    E    P    A    N    C    E    S        <<<<<<<<<', ord_repf)
       msg_out2(45*'>' + 45*'<' + '\n', ord_repf)
       msg_out2('   +++++++++  Asset Tags discrepances between Overseer and STD Systems  +++++++++\n', ord_repf)
       for k in lst_diff:
           msg_out2(k + ' <---> Overseer: ' + ser_oasset_d[k] + ', STD: ' + ser_asset_d[k], ord_repf)
       msg_out2(45*'>' + 45*'<' + '\n', ord_repf)
    elif len(vals) == 1 and vals[0] != 'Nothing':
       msg_out2(45*'>' + 45*'<', ord_repf)
       msg_out2('>>>>>>>>>  O_V_E_R_S_E_E_R :  A_S_S_E_T  T_A_G_S  A_R_E  O_K  W_I_T_H  S_T_D !!! <<<<<<<<<', ord_repf)
       msg_out2(45*'>' + 45*'<' + '\n', ord_repf)

    ################################
    #####   model Numbers   #####
    ################################
    conf_model_err_d = all_dicts['conf_model_err']
    if conf_model_err_d:
       model_ser_d = all_dicts['model_ser']
       msg_out2(5*'>' + 80*'=' + 5*'<', ord_repf)
       msg_out2('   +++++++++   Model  Number  D I S C R E P A N C E S  +++++++++\n', ord_repf)
       msg_out2(5*'>' + 80*'=' + 5*'<' + '\n', ord_repf)
       for conf in conf_model_err_d:
           msg_out2(' >>>> Configuration: ' + conf + ' <<<<', ord_repf)
           for model in conf_model_err_d[conf]:  
               msg_out2('   <++++ Model Number: ' + model + ' ++++> System Serials: ' + \
                        str(sorted(model_ser_d[model])).strip('[]'), ord_repf)
       msg_out2(5*'>' + 80*'=' + 5*'<'  + '\n', ord_repf)

    ################################
    #####   FAN  Information   #####
    ################################
    conf_fan_info_err_d = all_dicts['conf_fan_info_err']
    if conf_fan_info_err_d:
       fan_info_ser_d =  all_dicts['fan_info_ser']
       msg_out2(5*'>' + 80*'=' + 5*'<', ord_repf)
       msg_out2('   +++++++++  FAN  Information  D I S C R E P A N C E S  +++++++++', ord_repf)
       msg_out2(5*'>' + 80*'=' + 5*'<' + '\n', ord_repf)
       for conf in conf_fan_info_err_d:  # Here configs with 2 or more different FAN infos
           msg_out2(' >>>> Configuration: ' + conf + ' <<<<', ord_repf)
           lst_fan_info = conf_fan_info_err_d[conf]  # More then 1 elements (fan_infos)
           fan_str = lst_fan_info[0]
           fan_lst_0 = fan_str.split('@')
           msg_out2('  +++ For ' + str(len(fan_info_ser_d[fan_str])) + ' Systems: ' + str(sorted(fan_info_ser_d[fan_str])).strip('[]'), ord_repf)
           msg_out2('  ++++++ Here is the FAN Information taken as a template to compare: ', ord_repf)
           for s in fan_lst_0:
               msg_out2('        ' + s, ord_repf)

           for k in lst_fan_info[1:]:
               msg_out2('\n  +++ Differences in ' + str(len(fan_info_ser_d[k])) + ' Systems: ' + str(sorted(fan_info_ser_d[k])).strip('[]'), ord_repf)
               diff_lst = list(diff.compare(fan_lst_0,k.split('@')))
               for s in diff_lst:
                   msg_out2('      ' + s, ord_repf)
       msg_out2(5*'>' + 80*'=' + 5*'<'  + '\n', ord_repf)

    ################################
    #####   Dmidecode  Files   #####
    ################################
    conf_dmidec_err_d = all_dicts['conf_dmidec_err']
    if conf_dmidec_err_d:
       dmidec_ser_d =  all_dicts['dmidec_ser']
       msg_out2(5*'>' + 80*'=' + 5*'<', ord_repf)
       msg_out2('   +++++++++  Dmidecode Files  D I S C R E P A N C E S  +++++++++', ord_repf)
       msg_out2(5*'>' + 80*'=' + 5*'<' + '\n', ord_repf)
       for conf in conf_dmidec_err_d:                                # Here configs with 2 or more different dmidecode files
           msg_out2(' >>>> Configuration: ' + conf + ' <<<<', ord_repf)
           lst_dmidec = conf_dmidec_err_d[conf]  # More then 1 elements (dmidec)
           dmidec_str = lst_dmidec[0]                 # The first one taken as a template
           dmidec_lst_0 = dmidec_str.split('@')
           msg_out2('  +++ For ' + str(len(dmidec_ser_d[dmidec_str])) + ' Systems: ' + str(sorted(dmidec_ser_d[dmidec_str])).strip('[]'), ord_repf)
           msg_out2('  ++++++ The dmidecode file taken as a template to compare: ', ord_repf)

           for k in lst_dmidec[1:]:
               msg_out2('\n  +++ Differences in ' + str(len(dmidec_ser_d[k])) + ' Systems: ' + str(sorted(dmidec_ser_d[k])).strip('[]'), ord_repf)
               diff_lst = list(diff.compare(dmidec_lst_0, k.split('@')))
               for s in diff_lst:
                   if s[:2] == '  ': continue       # Skip all lines with no difference
                   # msg_out2('      ' + s, ord_repf)
                   msg_out2(s, ord_repf)
       msg_out2(5*'>' + 80*'=' + 5*'<'  + '\n', ord_repf)

    ################################
    ##### Hardware Information #####
    ################################
    report = ['-'*90, " Configuration".center(40) + "|" + "Total systems".center(15) + "|" + "Valid Systems".center(15) + "| Invalid", '-'*90]

    for conf in conf_ovsr_d:
	if not conf_ovsr_d[conf]:
	   report.append(" " + conf.ljust(38) + " | " + '0'.rjust(13) + " | " + '0'.rjust(13) + " | " + '0'.rjust(7))
	   continue
        max_sys_ovsr = conf_ovsr_d[conf][0]
        for ovsr in conf_ovsr_d[conf]:
            if len(ovsr_ser_dict[ovsr]) > len(ovsr_ser_dict[max_sys_ovsr]):
               max_sys_ovsr = ovsr
	
	conf_ser_lst = conf_ser_dict[conf]
	max_sys_lst = ovsr_ser_dict[max_sys_ovsr]
	total = len([k for k in conf_ser_lst if k not in ser_not_on_ovsr])
	most = len(max_sys_lst)
        diffs = total - most
	report.append(" " + conf.ljust(38) + " | " + str(total).rjust(13) + " | " + str(most).rjust(13) + " | " + str(diffs).rjust(7))
    report.append('-'*90 +'\n\n')
    
    msg_out2("\n        Burn-In Verification Report for Order: " + os.path.basename(ord_repf.name).replace('.txt',''), ord_repf)
    for line in report:
        msg_out2(line, ord_repf)


##########################################################################
# Compare systems only in STD DB
##########################################################################
def sys_compare(ord_repf, **all_dicts):
    print_db1_conf_distr( ord_repf, **all_dicts)
    conf_parts_d = all_dicts['conf_parts']
    parts_ser_dict = all_dicts['parts_ser']
    order = os.path.basename(ord_repf.name).replace('.txt','')

    for conf in conf_parts_d:
	if len(conf_parts_d[conf]) < 2:    # Amount of different part sets is 1 for this configuration
           parts = conf_parts_d[conf][0]   # Exactly one. Can't be none. That's why we use index 0
	   msg_out2('+++++ No differences in Configuration: ' + conf, ord_repf)
	   msg_out2(str(len(parts_ser_dict[parts])) + ' Systems: ' + str(sorted(parts_ser_dict[parts])).strip('[]'), ord_repf)
           print_parts(parts, parts, ord_repf)
	   msg_out2('-'*100 + '\n', ord_repf)
	else:
	   msg_out2('+++++ Differences in STD for Configuration: ' + conf, ord_repf)
           max_sys_parts = conf_parts_d[conf][0]  # Find a part set with the most systems in this configuration
	   for parts in conf_parts_d[conf]:
               if len(parts_ser_dict[parts]) > len(parts_ser_dict[max_sys_parts]):
                  max_sys_parts = parts

           for parts in conf_parts_d[conf]:
               msg_out2('Order: ' + order + ', Configuration: ' + conf, ord_repf)
	       tmp = 'Differences in '
	       if parts == max_sys_parts:
		  tmp = 'No differences in '
	       msg_out2(tmp + str(len(parts_ser_dict[parts])) + ' Systems: ' + str(sorted(parts_ser_dict[parts])).strip('[]'), ord_repf)
               print_parts(parts, max_sys_parts, ord_repf)
	       msg_out2('-'*100 + '\n', ord_repf)
 

##########################################################################
# Compare Overseer systems only
##########################################################################
def ovsr_sys_compare(ord_repf, **all_dicts):
    conf_ovsr_d = all_dicts['conf_ovsr']
    ovsr_ser_d = all_dicts['ovsr_ser']
    ser_ovsr_d = all_dicts['ser_ovsr']
    msg_out2('\n\n' + '+'*90 + '\n' + '+'*90, ord_repf)
    msg_out2("+++++++++++++++++++++++++++++++   O  V  E  R  S  E  E  R   +++++++++++++++++++++++++++++++", ord_repf)
    msg_out2('+'*90 + '\n' + '+'*90 + '\n\n', ord_repf)
    order = os.path.basename(ord_repf.name).replace('.txt','')
    
    if not ser_ovsr_d:
       msg_out2("  +++++!!!!  Nothing found in Overseer DB  !!!!++++\n", ord_repf)
       return

    print_ovsr_conf_distr(ord_repf, **all_dicts)

    for conf in conf_ovsr_d:
	if not conf_ovsr_d[conf]:
           msg_out2("+++++ No Overseer information for configuration " + conf, ord_repf)
	   msg_out2('-'*80 + '\n', ord_repf)
           # continue
	elif len(conf_ovsr_d[conf]) < 2:    # Amount of different part sets is 1 for this configuration
	   msg_out2('+++++ No differences on Overseer for Configuration: ' + conf, ord_repf)
           ovsr = conf_ovsr_d[conf][0]
           msg_out2(str(len(ovsr_ser_d[ovsr])) + ' Systems: ' + str(sorted(ovsr_ser_d[ovsr])).strip('[]'), ord_repf)
	   print_ovsr(ovsr, ovsr, ord_repf)
	   msg_out2('-'*80 + '\n', ord_repf)
	else:
           msg_out2('++++++ Differences on Overseer for Configuration: ' + conf, ord_repf)
           max_sys_ovsr = conf_ovsr_d[conf][0]
	   for ovsr in conf_ovsr_d[conf]:      # As a template config parts content we take one with maximum amount of systems having this content
               if len(ovsr_ser_d[ovsr]) > len(ovsr_ser_d[max_sys_ovsr]):
                  max_sys_ovsr = ovsr

           for ovsr in conf_ovsr_d[conf]:
               msg_out2('Order: ' + order + ', Configuration: ' + conf, ord_repf)
	       tmp = 'Differences in '
	       if ovsr == max_sys_ovsr:
		  tmp = 'No differences in '
	       msg_out2(tmp + str(len(ovsr_ser_d[ovsr])) + ' Systems: ' + str((sorted(ovsr_ser_d[ovsr]))).strip('[]'), ord_repf)
               print_ovsr(ovsr, max_sys_ovsr, ord_repf)
	       msg_out2('-'*80 + '\n', ord_repf)             


##########################################################################
# Iterate thru orders generating one report per order
##########################################################################
def iterate_orders(order_lst):
    global the_name

    for order in sorted(order_lst):
	cur_ord_rep = static_dir + the_name + '_' + order + ".txt"
	cur_ord_log = static_dir + the_name + '_'  + order + "_log.txt"
	ord_repf = open(cur_ord_rep, 'w')
	# ord_logf = open(cur_ord_log, 'w')
        # ord_logf_gl = ord_logf
        all_dicts = get_sys_serials(order)
	if all_dicts['ser_conf'].keys():
           print_dict(**all_dicts)
	   sys_compare(ord_repf, **all_dicts)
           ovsr_dicts = get_from_ovsr(order, **all_dicts)
           all_dicts.update(ovsr_dicts)
           print_ovsr_dict(**all_dicts)
	   ovsr_sys_compare(ord_repf, **all_dicts)
	else:
           msg_out2('  +++ There are no systems in the order ' + order + ' +++', ord_repf)
        ord_repf.close()
        # ord_logf.close()


##########################################################################
#
##########################################################################
def startmain():
    global logf, ssh_alex, db1_con, disk_dict, static_dir

    if len(sys.argv) < 3:
       print 'No orders.'
       exit(0)
    else:
       user = sys.argv[1]
       if not os.path.isdir(static_dir + user):
          print 'No directory in static for such user ', user
          exit(0)

       static_dir = static_dir + user + '/'
       orders = sys.argv[2:]

    hostalexs = 'alexs.alsher.com'

    logf = open(logfilename, 'w')
    db1_con = get_db_con(db1_db, db1_host, db1_user)
    mytools_con = get_db_con(mytools_db, mytools_host, mytools_user)
    order_lst, skipped = verify_orders(orders)
    
    if not order_lst and not skipped:
       msg_out("No such orders in STD: " + str(orders).strip('[]'))
       logf.close()
       db1_con.close()
       return

    if skipped:
       str1 = '\n Orders not found in STD DB: ' + str(skipped) + '\n\n'
       msg_out(str1)
       json.dump(skipped, logf)
    
    if not order_lst:
       close_all()
       return
    
    str1 = "\n     +++++++++++  Start to process orders " + str(sorted(order_lst)) + "   +++++++++\n"
    msg_out(str1 + '\n')
    remove_extra_files(os.path.dirname(logfilename), 64)
    remove_extra_files(static_dir, 50)
 
    disk_dict = read_disk_info()         # File disks.csv        --- dmesg descriptions
    iterate_orders(order_lst)
    close_all()
    

if __name__=="__main__":
    startmain()
