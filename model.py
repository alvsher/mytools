##################################################################################
#
# Alex Sherman
#    
###################################################################################

import web
import hashlib
from datetime import date, timedelta, datetime
import os
from config import *

#========= Database connection based on web.py features ======
# db=web.database(host='mydb.alsher.com', dbn='postgres', db='mydb2', user='mydb2')
db=web.database(dbn='postgres', host='tools.alsher.com', db='mytools', user='mydb2')

#=====================================================================================
users = {}           # User info from mytools
request_dct = {}     # Supported requests
login_dct = {}       # Every logined user will be in separate thread named with userid
menu_items = {}
search_items = {}
search_by_date = {}

######################################################################################
# User
######################################################################################

#=====================================================================================
# Extract user info from mytools and place into users as userid:{user info}
# id is an int number and admin is boolean True/False, others are ASCI strings
# str(dct[k]),dct[k])[k in ['id','admin'] -- str(dct[k]) if not in list ['id','admin']
#=====================================================================================
def get_users():
    rs=[dict(x) for x in db.query('select * from mytools_user')]
    d={str(dct['userid']): {k: (str(dct[k]),dct[k])[k in ['id','admin']] for k in dct} for dct in rs}

    return d


#=====================================================================================
# Return First/Last names and email from DB
#=====================================================================================
def get_user_by_userid(userid):
    try:
        x=db.select('mytools_user', where='userid=$userid', vars=locals())[0]
        return [str(x['fname']), str(x['lname']), str(x['email']), x['admin']]
    except IndexError:
        return ['None','None','None',False]


#=====================================================================================
# User info is in lst = [fname,lname,email,password, admin]. Salt for password is email
#=====================================================================================
def add_user(uid, dct):
    if uid in users:
        return 'You try to add User %s which is in the DB alredy.' % uid
    d = dict(dct)
    d['password'] = hashlib.sha1( d['password'] + d['email']).hexdigest()
    db.insert('mytools_user', userid=uid, fname=d['fname'], lname=d['lname'], email=d['email'], password=d['password'], admin=d['admin'])
      
    users[uid] = d
    return 'User %s added successfully.'


#=====================================================================================
# Delete user by userid.
#=====================================================================================
def del_user(userid):
    db.delete('mytools_user', where="userid=$userid", vars=locals())
    del users[userid]


#=====================================================================================
# User info is in dct with keys: fname,lname,email,password,admin.
# Salt for password is email
#=====================================================================================
def update_user(userid, dct):
    d = dict(dct)
    d['password'] = hashlib.sha1(d['password'] + d['email']).hexdigest()
    # pswd_str = hashlib.sha1(lst[3] + lst[2])
    db.update('mytools_user', where="userid=$userid", vars=locals(), **d)
    # users[userid] = {'fname':lst[0], 'lname':lst[1], 'email':lst[2], 'admin':lst[4], 'password':pswd_str}
    users[userid] = d


#=====================================================================================
# Hash password and place it into DB for the userid
#=====================================================================================
def change_pswd(pswd,userid):
    tmp1, tmp2, email = get_user_by_userid(userid)
    if email == 'None': return
    pswd_str = hashlib.sha1(pswd + email).hexdigest()
    db.update('mytools_user', where="userid=$userid", vars=locals(), password=pswd_str)
    usr_dct[userid]['password'] = pswd_str


#=====================================================================================
# Athentication retuns false if no such user or password is wrong
#=====================================================================================
def user_auth(userid,pswd):
    if userid in users and users[userid]['password'] == hashlib.sha1(pswd + users[userid]['email']).hexdigest():
        return True
    return False


######################################################################################
# Tools
######################################################################################

#=====================================================================================
# Execute Unix cmd and return mydbout 
#=====================================================================================
def get_by_cmd(cmd):
    ftemp = os.popen(cmd)
    return ftemp.read().rstrip("\n")


#=====================================================================================
# Delete earlier files if amount of files more than specified amount
#=====================================================================================
def remove_extra_files(dname, amount):

    lst_files = [str(k).strip() for k in get_by_cmd('ls -t ' + dname).split('\n')]
    if len(lst_files) > amount:
       rm_cmd = 'rm -f ' + ' '.join([dname + '/' + k for k in lst_files[amount:]])
       msg_out('rm_cmd: ' + rm_cmd)
       get_by_cmd(rm_cmd)


#=====================================================================================
# Refresh mytools database
#=====================================================================================
def refresh_mytools():
    return get_by_cmd(mytools_tools + 'mytools_refresh.sh')
    

#=====================================================================================
# Datetime in 2 formats. As timestamp and as string for filename construction
#=====================================================================================
def get_datetime_now():
    now = datetime.now()
    str_now_ms = now.strftime("%Y%m%dT") + now.strftime("%H%M%S%f")[0:8]
    return  now, str_now_ms


#=====================================================================================
#
#=====================================================================================
def mytools_dict_by_sql(sql_query):
    return [dict(x) for x in db.query(sql_query)]


#=====================================================================================
# Set defaults
# 'Disks To STD' : (14, ['Systems'], 'disks2mydb.py '),
# 'True/FreeNAS' : (18, ['From','To'], 'true_free_nas.sh ')
#=====================================================================================
def do_init():
    global users, request_dct, menu_items, search_items, search_by_date

    users = get_users()
    print users.keys()

    menu_items = {
            'Order Validation' : (10, ['Orders'], 'validation.py '),
            'Order CSV' : (15, ['Orders'], 'csv_order.py '),
            'Order System/Parts' : (20, ['Orders'], 'mytools_order_parts_ext.sh '),
            'Order Details' : (25, ['Orders'], 'mytools_order_parts_ext_details.sh '),
            'Order MACs' : (30, ['Orders'],'mytools_order_macs_ext.sh '),
            'Invoice Systems' : (35, ['Invoices'], 'mytools_invoice.sh '),
            'Invoice CSV' : (40, ['Invoices'], 'csv_invoice.py ')
            }

    search_items = {
            'System Parts' : (100, ['Systems'], 'mytools_sys_parts_ext.sh '),
            'System By Part' : (105, ['Part Serial'], 'mytools_sys_by_part_serial.sh '),
            'System By SM_Ser' : (110, ['SM_Serial'], 'mytools_sys_by_sm_serial.sh '),
            'System By MAC' : (115, ['MAC'], 'mytools_sys_by_mac.sh ')
            }
#    search_items = {
#            'Order' : (110, ['System','Order Number','MAC','Part Serial'], 'search_order.sh '),
#            'Invoice' : (115, ['System','Invoice','Order Number','Customer'], 'search_invoice.sh '),
#            'System' : (120, ['System Serial','Part Serial','SM_Serial','Asset Tag','MAC'], 'search_system.sh '),
#            'Part' : (125, ['Serial','Model','Manufacturer'], 'search_part.sh '),
#            'System File' : (130, ['sysinfo','dmidecode','dmesg','disk_info'], 'search_file.sh '),
#            'Customer' : (135, ['Customer','Order','System','Part Serial'], 'search_customer.sh '),
#            'Customer By Date' : (140, ['Orders','Invoices','Systems','Parts'],'mytools_sys_dmesg.sh ')
#            }
    search_by_date = {
            'Customer Systems' : (210, ['Customer','From','To'], 'ct_sys_dt_search.sh'),
            'Customer Parts'   : (215, ['Customer','From','To'], 'ct_prt_dt_search.sh'),
            'TrueNAS Systems'  : (220, ['From','To'], 'truenas_dt_search.sh'),
            'FreeNAS Systems'  : (225, ['From','To'], 'freenas_dt_search.sh'),
            'NAS Systems'      : (230, ['From','To'], 'nas_dt_search.sh')
            }

    request_dct = { 
            'refresh_db':'mytools_refresh.sh',
            'mac_dir':'mytools_mac_dir.sh', 
            'order_csv_gen':'order_csv_gen.sh',
            'invoice_csv_gen':'invoice_csv_gen.sh',
            'sys_disk2mydb':'sys_disk2mydb.sh',
            'sys_parts':'mytools_sys_parts_ext.sh',
            'order_parts':'mytools_order_parts_ext.sh',
            'sys_mem_size':'mytools_sys_mem_size.sh'
            }
    login_dct = {u : {} for u in users}

