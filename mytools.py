#!/usr/bin/env /usr/local/bin/python
##################################################################################
#
# Alex Sherman
#    
###################################################################################

# from datetime import date, timedelta, datetime
import web
from web import form
from model import *
import paramiko
import os
import operator
from config import *

#=================================================================
#      ++++++ Lookup dictionaries ++++++
#=================================================================
do_init()
web.config.smtp_server = "mail.alsher.com"
#=================================================================

user='nobody'
ssh_alexs = None

render = web.template.render('templates/', cache=False)

urls = ('/', 'index', 
        '/confirm','confirm', 
        '/logout','logout',
        '/chng_pswd','chng_pswd',
        '/mytools','mytools',
        '/admin','admin',
        '/images/(.*)', 'images'
       )

app = web.application(urls, locals())

login = form.Form(
    form.Textbox('username'),
    form.Password('password'),
    form.Button('Login')
)

myform = form.Form(
    form.Textarea(name='Notes', rows=10, cols=60)
)

user_form = form.Form(
    form.Textbox("User ID"),
    form.Textbox("First Name"),
    form.Textbox("Last Name"),
    form.Textbox("Email"),
    form.Checkbox("Admin", description="Admin Rights",class_="standard", value="something.. Anything!")
)

confirm_form = form.Form(
    form.Button('OK'),
    form.Button('Cancel')
)


##########################################################################
#
##########################################################################
class images:
    def GET(self,name):
        ext = name.split(".")[-1] # Gather extension

        cType = {
            "png":"images/png",
            "jpg":"images/jpeg",
            "gif":"images/gif",
            "ico":"images/x-icon"            }

        if name in os.listdir('images'):  # Security
            web.header("Content-Type", cType[ext]) # Set the Header
            return open('images/%s'%name,"rb").read() # Notice 'rb' for reading images
        else:
            raise web.notfound()


##########################################################################
# Change Password Form to process
##########################################################################
class chng_pswd:
    def POST(self):
        form = myform()
        info = web.input()
        userid = info.user_id.strip()
        user_dct = users[userid]
        user_email = user_dct['email']
        btn = info.btn

        if btn == "Submit Changes":
           # Anybody can change own password
           #
           old_pswd = hashlib.sha1(info.old_pswd + user_email).hexdigest()
           new_pswd1 = info.new_pswd1
           new_pswd2 = info.new_pswd2
           if old_pswd != user_dct['password']:
               msg = "Current password is incorrect. Try again."
               return render.mytools_change_pswd(form, msg, **user_dct)
           elif new_pswd1 != new_pswd2:
               msg = "Entered new passwords are different. Should be the same."
               return render.mytools_change_pswd(form, msg, **user_id)
           else:
               self.change_pswd(new_pswd1 + user_email, userid)
               raise web.seeother('/')
        elif btn == "Reset Password":
           # Admin can reset password to default for any employee userid
           #
           emp_userid = info.emp_userid.strip()
           if emp_userid not in users:
               msg = "User '%s' is not valid" % emp_userid
               return render.mytools_change_pswd(form, msg, **user_dct)
           else:
               emp_email = users[emp_userid]['email']
               self.change_pswd('abcd1234' + emp_email,emp_userid)
               raise web.seeother('/')
        else:
            # It is btn == "Cancel"
            #
            raise web.seeother('/')


    def change_pswd(self,pswd,userid):     
       pswd_str = hashlib.sha1(pswd).hexdigest()
       db.query("UPDATE mytools_user SET password = '%s' where userid = '%s'" % (pswd_str,userid))
       users[userid]['password'] = pswd_str



##########################################################################
# Class to run 1 user independently
##########################################################################
class User:

    ##########################################################################
    # Constructor
    ##########################################################################
    def __init__(self, user_dct):
        self.u_id = user_dct['userid']
        self.u_email = user_dct['email']
        self.u_dct = dict(user_dct)             # Make own copy of user_dct

    def get_user_dct(self):
        return self.u_dct

    def set_to_dct(key, val):
        self.u_dct[key] = val


##########################################################################
# Class to add/update/delete user 
##########################################################################
class admin:
    def POST(self):
        form = myform()
        info = web.input()
        login_userid  = str(info.login_userid)
        user_obj = login_dct[login_userid]
        user_id = str(info.user_id).strip()
        first_name = str(info.fname).strip()
        last_name = str(info.lname).strip()
        user_email = str(info.email1).strip()
        user_pswd = str(info.password)
        # admin_rights = False
        # if 'admin' in dict(info): admin_rights = True
        admin_rights = ('admin' in dict(info))
        init_dct = {'userid':'', 'fname':'', 'lname':'', 'email':'', 'user_email':'', 'password':'abcd1234', 'admin':False}
        user_dct = {'userid':user_id, 'fname':first_name, 'lname':last_name, 'email':user_email, 'password':'abcd1234', 'admin':admin_rights}
        kw = {'login_userid':login_userid, 'cur_user':user_dct}
        btn = info.btn
        if 'Add User' in btn:
            print 'user_id == ', user_id
            print 'user_dct == ', user_dct
            msg = add_user(user_id,user_dct)
            user_dct['user_email']=user_dct['email']
            #db.insert('mytools_user', userid=user_id, fname=first_name, lname=last_name, email = user_email, \
            #          password = hashlib.sha1(user_pswd + user_email).hexdigest(), admin = admin_rights)
            kw['msg'] = msg
            kw['cur_user'] = user_dct
            return render.mytools_admin(form, **kw)
        if 'Fill By User ID' in btn:
            # first_name, last_name, user_email, admin_rights = get_user_by_userid(user_id)
            user_dct = users.get(user_id,init_dct)
            user_dct['password']='abcd1234'
            user_dct['user_email']=user_dct['email']
            kw['cur_user'] = user_dct
            kw['msg']='Working on user ' + user_id
            return render.mytools_admin(form, **kw)
        elif 'Update User' in btn:
            # lst=[first_name,last_name,user_email,user_pswd,admin_rights]
            update_user(user_id, user_dct)
            user_dct['user_email']=user_dct['email']
            return render.mytools_admin(form, **kw)
        elif 'Delete User' in btn:
            del_user(user_id)
            kw['cur_user']=init_dct
            kw['msg']='User ' + user_id + ' Sucessfully Deleted.'
            return render.mytools_admin(form, **kw)
        elif 'Cancel' in btn:
            return render.mytools_base(form, user_obj)
        else:
            return render.mytools_base(form, user_obj)


##########################################################################
# Class to run 1 user independently
##########################################################################
class mytools:
    def POST(self):
        form = myform()
        info = web.input()
        userid  = str(info.login_userid)
        user_obj = login_dct[userid]
        u_dct = user_obj.get_user_dct()
        u_dct['info'] = info
        btn = info.btn
        if btn == 'Admin Users':
            user_dct = {'userid':"",'fname':"", 'lname':"", 'email':"", 'user_email':"", 'password':"abcd1234", 'admin':False}
            kw = {'login_userid':userid, 'msg':"Please Fill The Fields", 'cur_user':user_dct}
            return render.mytools_admin(form, **kw)
            # u_dct['curr_action'] = 'Refresh Database'
            # u_dct['notes'] = 'Refreshing database mytools'
            # refresh_mytools()            # The function is in model
            #return render.mytools_base(form, user_obj)
        elif btn == 'Email Results':
            u_dct['curr_action'] = 'Email Results'
            u_dct['notes'] = 'Email Sent To %s' % u_dct['email']
            u_dct['lst_input'] = []
            m_from = 'alexs@alsher.com'
            m_to = [u_dct['email']]
            csv_name = os.path.basename(u_dct['to_print'].replace('.html','.csv'))
            file_attachment = os.path.basename(u_dct['to_print'])
            if os.path.isfile(u_dct['user_static_dir'] + csv_name):
               file_attachment = csv_name
            m_subject = 'MYTOOLS file attached %s' % file_attachment
            m_content = 'Login time %s.\n\n%s' % (u_dct['logintime'], u_dct['user_static_dir'].replace(mytools_dir,'http://alexs.alsher.com:9200/'))
            m_attachment = u_dct['user_static_dir'] + file_attachment
            web.sendmail(m_from, m_to, m_subject, m_content, attachments=[m_attachment])
            return render.mytools_base(form, user_obj)
        elif btn == 'Logout':
            login_dct[userid] = None
            raise web.seeother('/')
        elif btn in menu_items:
            u_dct['is_menu'], u_dct['is_search'], u_dct['is_date_search'] = True, False, False
            u_dct['notes'] = u_dct['menu_action'] = u_dct['curr_action'] = btn
            u_dct['lst_input'] = menu_items[btn][1]
            return render.mytools_base(form, user_obj)
        elif btn in search_items:
            u_dct['is_menu'], u_dct['is_search'], u_dct['is_date_search']  = False, True, False
            u_dct['notes'] = u_dct['search_item'] = u_dct['curr_action'] = btn
            u_dct['lst_input'] = search_items[btn][1]
            return render.mytools_base(form, user_obj)
        elif btn in search_by_date:
            u_dct['is_menu'], u_dct['is_search'], u_dct['is_date_search']  = False, False, True
            u_dct['notes'] = u_dct['by_date_item'] = u_dct['curr_action'] = btn
            u_dct['lst_input'] = search_by_date[btn][1]
            return render.mytools_base(form, user_obj)
        elif btn == 'Submit':
            result = self.process_submit(u_dct)
            u_dct['to_print'] = u_dct['result_dir'] + result
            return render.mytools_base(form, user_obj)
        else:
            raise web.seeother('/')


    ##########################################################################
    # Submit button with text field values for apps
    ##########################################################################
    def process_submit(self,u_dct):
        info = u_dct['info']
        action = u_dct['curr_action']
        userid = u_dct['userid']
        if u_dct['is_menu']:
           textfield_name = menu_items[action][1][0]
           u_dct[textfield_name]=info[textfield_name]
           str_params = ' '.join(info[textfield_name].strip().replace(' ','').split(','))
           apps_to_execute = mytools_apps + menu_items[action][2] + userid + ' ' + str_params
           get_by_cmd(apps_to_execute)
        elif u_dct['is_search']:
           str_params = ' ' + userid + ' '
           field_lst = search_items[action][1]
           for k in field_lst:
              str_params += '\'' + info[k].strip() + '\' '
           apps_to_execute = mytools_apps + search_items[action][2] + str_params
           get_by_cmd(apps_to_execute)
        elif u_dct['is_date_search']:
           str_params = ' ' + userid + ' '
           field_lst = search_by_date[action][1]
           for k in field_lst:
              str_params += '\'' + info[k].strip() + '\' '
           apps_to_execute = mytools_apps + search_by_date[action][2] + str_params
           get_by_cmd(apps_to_execute)

        result = get_by_cmd('ls -tr ' + u_dct['user_static_dir'] + ' | tail -1')
        u_dct['notes'] = action + ': ' + result
        return  result

 
    ##########################################################################
    # Search
    ##########################################################################
    def search(self,u_dct):
        info = u_dct['info']
        search_item = u_dct['search_item']                          # search_item: Order, Invoice, System, Part, ...
        userid = u_dct['userid'] 
        radio_btn_name = info['search_by'].replace(' ','_')         # The name of radio button is search_by
        search_val = u_dct['search_value'] = info['search_value']   # The name of search textfiled is search_value
        print 'textfield value: ', search_val
        str_params = search_item + ' ' + radio_btn_name + ' ' + search_val
        print str_params
        apps_to_execute = mytools_apps + search_items[search_item][2] + userid + ' ' + str_params
        print '\n=================\n'
        print apps_to_execute
        get_by_cmd(apps_to_execute)
        result = get_by_cmd('ls -tr ' + u_dct['user_static_dir'] + ' | tail -1')
        u_dct['notes'] = search_item + ': ' + result
        return  result


##########################################################################
# OK and CANCEL page instead of javascript modal
##########################################################################
class confirm:
    def POST(self):
        form = myform()
        info = web.input()
        userid = info.user_id
        print userid
        user_obj = login_dct[userid]
        u_dct = user_obj.get_user_dct()
        if info.btn == 'Cancel':
            u_dct['notes'] = 'You clicked cancel'
            return render.mytools_base(form, user_obj)

        if u_dct['curr_action'] == 'Logout':
            login_dct[userid] = None
            # user_dct = users['nobody']
            # user_dct['warning'] = "Please input your userid and password."
            # return render.mytools_login(form, **user_dct)
            raise web.seeother('/')
        else:
            u_dct['notes'] = 'Unknown Action'
            return render.mytools_base(form, user_obj)


#=====================================================================================
# SSH to pbs with key_gen private key. No password needed
#=====================================================================================
def connect_alexs():
   global ssh_alexs
   for i in range(3):
      try:
         pk = paramiko.RSAKey.from_private_key_file("/usr/home/alexs/.ssh/id_rsa")
         ssh_alexs = paramiko.SSHClient()
         ssh_alexs.set_missing_host_key_policy(paramiko.AutoAddPolicy())
         ssh_alexs.connect( hostname = "work.alsher.com", username = "alexs", pkey = pk )
         break
      except:
         pass


#=====================================================================================
# Execute Unix cmd on alexs and return stdout 
#=====================================================================================
def get_by_cmd_from_alexs(cmd):
   if not ssh_alexs:
      connect_alexs()

   lst = []
   for i in range(2): 
      try:
          stdin, stdout, stderr = ssh_alexs.exec_command(cmd)
          lst = [str(k).rstrip('\n') for k in stdout.readlines()]
          break
      except:
          lst = ["ERROR: Connection Failure to PROD1"]
      connect_alexs()       # Try to connect again

   return lst, cmd


#==============================================================================================================
####################################################################################
# Write into STD DB with time stamp, default is NOW()
####################################################################################
def do_login(user_dct):
    userid = user_dct['userid']
    datetimetag = datetime.today().strftime("%Y-%m-%d %H:%M:%S")

    if  userid == 'nobody':
        user_dct['warning'] = 'Login User "' + userid + '" is not valid.'
        user_dct['logined'] = False
    else:
        user_dct['warning'] = 'You logged in as ' + userid + '\nYour name is: ' +  user_dct['fname'] + ' ' + user_dct['lname']
        user_dct['logined'] = True
        user_dct['logintime'] = datetimetag
        user_dct['curr_action'] = 'logined'
        user_dct['menu_action'] = 'Login: ' + userid
        user_dct['notes'] = 'Login: ' + userid
        user_dct['lst_input'] = []
        user_dct['menu_items'] = [k[0] for k in sorted(menu_items.items(), key=operator.itemgetter(1))]
        user_dct['search_items'] = [k[0] for k in sorted(search_items.items(), key=operator.itemgetter(1))]
        user_dct['search_by_date'] = [k[0] for k in sorted(search_by_date.items(), key=operator.itemgetter(1))]
        user_dct['user_static_dir'] = dir_name = mytools_static + userid + '/'    # Absolute path to user static dir
        user_dct['result_dir'] = '/static/' + userid + '/'       # The result_dir is not absolute path. Should be like /static/alexs/
        if not os.path.isdir(dir_name):   
            os.makedirs(dir_name)          # all result outputs will be placed here
            get_by_cmd('touch ' + dir_name + 'dummy.txt')

        user_dct['to_print'] = user_dct['result_dir'] + get_by_cmd('ls -tr ' + user_dct['user_static_dir'] + ' | tail -1') 
        # user_dct['lst_input'] = ['nothing', 'nothing1']

    login_dct[userid] = User(user_dct)
    # sql_insert = "INSERT INTO mytools_login VALUES(default, '%s', '%s')" % (userid, datetimetag, datetimetag)
    # db.query(sql_insert)


####################################################################################
#
####################################################################################



####################################################################################
# Main class to login into the system
####################################################################################
class index:
    global users
    def GET(self): 
        form = myform()
        # make sure you create a copy of the form by calling it (line above)
        # Otherwise changes will appear globally
        user_dct = users['nobody']
        user_dct['warning'] = "Please input your userid and password."
        return render.mytools_login(form, **user_dct)

    def POST(self):
        user_dct = users['nobody']
        form = myform() 
        user_data = web.input()
        userid = user_data['user_id'].strip()
        if userid not in users or userid.lower() == 'nobody':
           user_dct['warning'] = "User '%s' is not valid." % userid
           return render.mytools_login(form, **user_dct)
      
        btn  = user_data.btn
        user_dct = users[userid]
        user_email = user_dct['email']
        if btn == 'Change Password':
           msg = "Userid %s. Change Password." % userid
           return render.mytools_change_pswd(form, msg, **user_dct)
        elif btn == 'Login':
           pswd = hashlib.sha1(user_data['password'] + user_email).hexdigest()
           if user_dct['password'] == pswd:
              do_login(user_dct)
              return render.mytools_base(form,login_dct[userid])
           else:
              user_dct['warning'] = 'Userid %s: Password is not valid.' % userid
        return render.mytools_login(form, **user_dct)

if __name__=="__main__":
    web.internalerror = web.debugerror
    app.run()
