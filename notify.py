#######################################################################################
#
# Alex Sherman
#
#######################################################################################

import sys
import os
import json
import psycopg2

import paramiko
import base64
import math

import time
from datetime import date, timedelta, datetime
from dateutil import parser

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

from email.parser import HeaderParser
import imaplib, email, re

from collections import Counter

import web
from web import form
import model
from model import *

web.config.smtp_server = 'mail.alsher.com'


sql_lg_host = 'db.alsher.com'
sql_lg_db = 'sql-ledger'
sql_lg_user = 'vpn_ro'
sql_lg_con = None

ovsr_con_str = "psql overseer overseer << EOF\n"  # From prod1.alsher.com (dbname and user)
sql_lg_con_str = "psql -h db.alsher.com -U vpn_ro -d sql-ledger << EOF\n"
db1_con_str = "psql -h db1.alsher.com -U db12 -d db12 << EOF\n"
eof_str = ";\nEOF"

#=================================================================
#      ++++++ Lookup dictionaries ++++++
#=================================================================

emp_dct, step_dct, issue_d_c, route_dct, issue_c_d, step_time_dct = read_from_db1()

lst_dct = [emp_dct, step_dct, issue_d_c, route_dct, issue_c_d, step_time_dct]
#=================================================================

users = {uid: dict(do_init(uid)) for uid in emp_dct}
users_in = users_out = {}

render = web.template.render('templates/', cache=False)

urls = ('/', 'index')
app = web.application(urls, locals())

myform = form.Form(
    form.Textarea(name='Notes', rows=10, cols=60))

myform1 = form.Form(
    form.Textbox("User ID"),
    form.Textbox("First Name"),
    form.Textbox("Last Name"),
    form.Textbox("Email"),
    form.Textarea('Description',rows=8, cols=40))

#=================================================================
#      ++++++ Constants ++++++
#=================================================================
today_date = date.today().strftime("%Y-%m-%d")
yesterday = (date.today() - timedelta(days=1)).strftime("%Y-%m-%d")
current = datetime.today()
prior_30hrs = current - timedelta(hours=30)
datetimetag = current.strftime("%Y-%m-%d %H:%M:%S")

dt_0906 = today_date + " 09:06:00"
dt_1031 = today_date + " 10:31:00"
dt_1050 = today_date + " 10:50:00"
dt_1401 = today_date + " 14:01:00"
dt_1531 = today_date + " 15:31:00"
dt_1550 = today_date + " 15:50:00"
dt_1759 = today_date + " 17:59:00"
cdt_0906 = datetime.strptime(dt_0906, "%Y-%m-%d %H:%M:%S")
cdt_1031 = datetime.strptime(dt_1031, "%Y-%m-%d %H:%M:%S")
cdt_1050 = datetime.strptime(dt_1050, "%Y-%m-%d %H:%M:%S")
cdt_1401 = datetime.strptime(dt_1401, "%Y-%m-%d %H:%M:%S")
cdt_1531 = datetime.strptime(dt_1531, "%Y-%m-%d %H:%M:%S")
cdt_1550 = datetime.strptime(dt_1550, "%Y-%m-%d %H:%M:%S")
cdt_1759 = datetime.strptime(dt_1759, "%Y-%m-%d %H:%M:%S")

minutes_0 = timedelta(minutes=0)
minutes_1 = timedelta(minutes=1)
minutes_15 = timedelta(minutes=15)
minutes_20 = timedelta(minutes=20)
minutes_30 = timedelta(minutes=30)
minutes_1440 = timedelta(minutes=1440)
minutes_1800 = timedelta(minutes=1800)

# hostalex = 'asherman.alsher.com'
hostalex = 'localhost'
home_dir = '/usr/home/asherman/projects/wf_muser/'
subdir = str(current)[:10] + '/'
logfilename = home_dir + 'logs/' + current.strftime("%Y%m%d-%H%M%S") + '.log'
rpt_basename = current.strftime("%Y%m%d-%H%M%S") + '.txt'
rptfile = home_dir + 'rpts/' + rpt_basename
static_dir =  home_dir + 'static/'
static_today = static_dir + today_date + '/'
host_prod1 = 'prod1.alsher.com'
logf = None
rptf = None

user = base64.b64decode('WVhOb1pYSnRZVzQ9')
passwd = base64.b64decode('VEhWaVlUQXdNREE9')
ssh_alex  = None
ssh_prod1 = None

emails = ['asherman@alsher.com', 'chris@alsher.com', 'vu@alsher.com', 'kham@alsher.com', 'andrewn@alsher.com', 'gil@alsher.com']
to_me = 'asherman@alsher.com'

# diff_last_2_rpt = "diff `ls -tr %s*.txt | tail -2 | cut -d '>' -f2 | tr '\\n' ' '`" % static_subdir
diff_last_2_rpt = "if [ ! -z `ls | wc -l` ]; then diff `ls -tr %s*.txt | tail -2 | cut -d '>' -f2 | tr '\\n' ' '`; fi" % static_subdir
#=================================================================

login_emp_dct = {}
break_emp_dct = {}
indirect_dct  = {}

#=================================================================

##################################################
# Error message printing in files and db1out
##################################################
def msg_out(msg):
    print msg
    logf.write(msg + '\n')


##################################################
# Message printing in rptf file
##################################################
def rpt_str(msg):
    msg_out(msg)
    rptf.write(msg + '\n')


##################################################
# Get value by Unix cmd
##################################################
def get_by_cmd(cmd):
    ftemp = os.popen(cmd)
    return ftemp.read().rstrip("\n")


#############################################################
# Get from DB by psql << EOF with SQL statement as input
#############################################################
def select_from_db1_no_con(sql1):
    cmd = db1_con_str + sql1 + eof_str
    return get_by_cmd(cmd).split('\n')[2:]


#############################################################
# It works as this example:  Feb 12, 2015  ==> 2015-02-12
#############################################################
def get_db_date_format(dt):
    return str(parser.parse(dt))[0:10]


#############################################################
# Get connection to DB
#############################################################
def get_db_con(db, hst, usr):
    con = None
    try:
        con = psycopg2.connect(database=db, host=hst, user=usr)
    except psycopg2.DatabaseError, e:
        msg_out('\nError ' + str(e))
    return con


##################################################
# Get N days DAYTAG
##################################################
def get_n_days_daytag(n):
    dtags = []
    for i in range(0,n):
        dtags.append(str(date.today() - timedelta(days=i)).replace("-", ""))
    return dtags


#############################################################
# Get ssh connection to a host with hostname as a parameter
#############################################################
def get_ssh_con(hostname):

    ssh_con = paramiko.SSHClient()
    ssh_con.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_con.connect(hostname, username=base64.b64decode(user), password=base64.b64decode(passwd))

    return  ssh_con


##########################################################################
# Delete earlier files if amount of files more than specified amount
##########################################################################
def remove_extra_files(dname, amount):

    lst_files = [str(k).strip() for k in get_by_cmd('ls -t ' + dname).split('\n')]
    if len(lst_files) > amount:
       rm_cmd = 'rm -f ' + ' '.join([dname + k for k in lst_files[amount:]])
       msg_out('rm_cmd: ' + rm_cmd)
       get_by_cmd(rm_cmd)


####################################################################################
# Last processed record on workflow_process for system serial as dictionary in list
# The list is empty if such record does not exist (system was not processed at all)
####################################################################################
def get_sys_from_process(sys_ser):

    sql1 = "select * from workflow_process where system_serial = '%s' order by out_stamp desc limit 1" % sys_ser
    return [dict(x) for x in db.query(sql1)]


##########################################################################
# Employees who login today.
##########################################################################
def get_today_logins():

    sql_login = "select userid, date_trunc('minute',login)::timestamp login_tm, date_trunc('minute',logout)::timestamp logout_tm from workflow_login " + \
                "where date_trunc('day',login)::timestamp = timestamp '%s'" % today_date

    rs = [dict(r) for r in db.query(sql_login)]
    emps = list(set([k['userid'] for k in rs]))

    # logins_dct = { k : [(x[1],x[2]) for x in rs if x[0] == k] for k in emp_dct}
    logins_dct = {k : [(datetime.strptime(str(x['login_tm']), "%Y-%m-%d %H:%M:%S"), \
                  datetime.strptime(str(x['logout_tm']), "%Y-%m-%d %H:%M:%S")) for x in rs if x['userid'] == k] for k in emps}  # no empty lists

    return logins_dct       # Login today {userid : [(login,logout), ...]}


##########################################################################
# Employees who login today. Return dict {userid : [(br_bgn,br_end), ...]}
##########################################################################
def get_today_breaks():

    sql_break = "select userid, date_trunc('minute',br_bgn)::timestamp br_bgn, date_trunc('minute',br_end)::timestamp br_end from workflow_break " + \
                "where date_trunc('day',br_bgn)::timestamp = timestamp '%s'" % today_date

    rs = [dict(r) for r in db.query(sql_break)]
    emps = list(set([k['userid'] for k in rs]))

    breaks_dct = {k : [(datetime.strptime(str(x['br_bgn']), "%Y-%m-%d %H:%M:%S"), \
                  datetime.strptime(str(x['br_end']), "%Y-%m-%d %H:%M:%S")) for x in rs if x['userid'] == k] for k in emps}  # no empty lists

    return  breaks_dct         # All breaks taken today {userid : [(br_bgn,br_end), ...]}

##########################################################################
# Return dict {userid : [(it_bgn,it_end), ...]}
##########################################################################
def get_today_indirect():

    sql_indirect = "select userid, date_trunc('minute',it_bgn)::timestamp it_bgn, date_trunc('minute',it_end)::timestamp it_end " + \
                   "from workflow_indirect where date_trunc('day',it_bgn)::timestamp = timestamp '%s'" % today_date

    rs = [dict(r) for r in db.query(sql_indirect)]
    emps = list(set([k['userid'] for k in rs]))

    indirect_d = {k : [(datetime.strptime(str(x['it_bgn']), "%Y-%m-%d %H:%M:%S"), \
                  datetime.strptime(str(x['it_end']), "%Y-%m-%d %H:%M:%S")) for x in rs if x['userid'] == k] for k in emps}

    return  indirect_d         # All indirect activity today {userid : [(br_bgn,br_end), ...]}


##########################################################################
# Employee does not login in by 9:05.
##########################################################################
def get_not_login_by_905():

    if current < cdt_0906:
        return []

    login_by_905_lst = []
    for k in login_emp_dct:
        lst = sorted(login_emp_dct[k])
        l_tup = lst[0]
        if l_tup[0] < cdt_0906:
           login_by_905_lst.append(k)

    return [k for k in emp_dct if k not in login_by_905_lst]  # Not login before 9:05 employee list


##########################################################################
# Employee does not take a break by 10:30
##########################################################################
def get_brk_after_1030():

    if current < cdt_1031:
        return []

    break_by_1030_lst = []
    for k in break_emp_dct:
        lst = sorted(break_emp_dct[k])
        l_tup = lst[0]
        if l_tup[0] < cdt_1031:
            break_by_1030_lst.append(k)

    return [k for k in login_emp_dct if k not in break_by_1030_lst]  # Did not take break before 10:30 logged-in employees


##########################################################################
# Employee took first break longer than 20 minutes
##########################################################################
def get_brk1_longer_than_20():

    if current < cdt_1050:
        return {}

    emp_brk1_dct = {k : break_emp_dct[k][0] for k in break_emp_dct if break_emp_dct[k]}

    brk1_longer_than_20_dct = {k : emp_brk1_dct[k] for k in emp_brk1_dct if emp_brk1_dct[k][0] + minutes_20 > emp_brk1_dct[k][1]}

    return brk1_longer_than_20_dct


##########################################################################
# employee takes single cumulative break less then 30 minutes by 2pm
##########################################################################
def get_no_brk_over_30_before_2pm():
    
    if current < cdt_1401:
        return []

    emp_less_than_30_brk = []
    for k in login_emp_dct:
        if k not in break_emp_dct:
           emp_less_than_30_brk.append(k)
           continue

        t_sum = minutes_0
        for x in break_emp_dct[k]:     # In array of tuples
            if x[0] < cdt_1401:        # Before 2PM
                t_sum += x[1] - x[0]

        if t_sum < minutes_30:
            emp_less_than_30_brk.append(k)

    return emp_less_than_30_brk


##########################################################################
# Employee does not take second break by 03:30
##########################################################################
def get_brk2_after_1530():

    if current < cdt_1531:
        return []

    brk2_by_1530_lst = []
    for k in break_emp_dct:
        lst = sorted(break_emp_dct[k])
        if len(lst) < 2:
            continue

        l_tup = lst[1]
        if l_tup[0] < cdt_1531:
            brk2_by_1530_lst.append(k)

    return [k for k in login_emp_dct if k not in brk2_by_1530_lst]  # Did not take second break before 15:30 logged-in employees


##########################################################################
# Employee took second break longer than 20 minutes
##########################################################################
def get_brk2_longer_than_20():

    if current < cdt_1550:
        return {}

    emp_brk2_dct = {k : break_emp_dct[k][1] for k in break_emp_dct if len(break_emp_dct[k]) > 1 }

    brk2_longer_than_20_dct = {k : emp_brk2_dct[k] for k in emp_brk2_dct if emp_brk2_dct[k][0] + minutes_20 > emp_brk2_dct[k][1]}

    return brk2_longer_than_20_dct


##########################################################################
# Employee logs out with less than 8 cumulative hours (including breaks)
# Calculate in minutes. 8 hours = 480 minutes
##########################################################################
def get_less_than_8_hours_login():

    emp_total_minutes_dct = {}
    for k in login_emp_dct:
        lst_tup = sorted(login_emp_dct[k])
        if lst_tup[-1][1] == lst_tup[-1][0]:   # Employee did not logout yet
            continue

        login_sum = minutes_0
        for x in lst_tup:     # In array of tuples
            login_sum += x[1] - x[0]

        brk_sum = minutes_0
        if k in break_emp_dct:
            for x in break_emp_dct[k]:
                brk_sum += x[1] - x[0]

        emp_total_minutes_dct[k] = getattr(login_sum - brk_sum, 'seconds')//60      # 8 hours = 480 minutes

    return {k : emp_total_minutes_dct[k] for k in emp_total_minutes_dct if emp_total_minutes_dct[k] < 480}


##########################################################################
# employee is idle for 15 minutes
##########################################################################
def get_idle_15_min():

    sql_active = "select userid, date_trunc('minute',in_stamp)::timestamp _in_, date_trunc('minute',out_stamp)::timestamp _out_, active from workflow_process " + \
                 "where date_trunc('day', in_stamp)::timestamp = timestamp '%s'" % today_date

    rs = [dict(r) for r in db.query(sql_active)]

    worked_lst = list(set([r['userid'] for r in rs]))                     # List of userid worked on systems today
    not_worked_lst = list(set(login_emp_dct.keys()) - set(worked_lst))    # List of userid logged in but did not work

    # Dict of worked on systems today
    # {userid : [(in_stamp, out_stamp, active), ...]}
    rs_worked_dct = {str(k) : [(r['_in_'], r['_out_'], r['active']) for r in rs if r['userid'] == k] for k in worked_lst}

    rs_idle_dct = dict(rs_worked_dct)                                     # Dict of idle now (finished all systems)
    for k in rs_worked_dct:
        active_lst = [r[0] for r in rs_worked_dct[k] if r[2] == True]
        if active_lst:                                                    # Empty list means that userid is idle
            del rs_idle_dct[k]

    idle_15_not_worked = []                                               # Logged in, not worked, idle more than 15
    for k in not_worked_lst:
        lst = sorted(login_emp_dct[k])
        last_login_time = lst[-1][0]
        if last_login_time + minutes_15 < current:
           idle_15_not_worked.append(k)

    idle_15_worked = []                                                   # Finished with all systems, idle more than 15
    for k in rs_idle_dct:
        lst = sorted(rs_idle_dct[k])
        last_out_stamp = lst[-1][1]
        if last_out_stamp + minutes_15 < current:
            idle_15_worked.append(k)

    lst_idle_15 = idle_15_worked
    if idle_15_not_worked:
        lst_idle_15.extend(idle_15_not_worked)

    if lst_idle_15:
        lst_idle_15 = list(set(lst_idle_15))
    
    return lst_idle_15


##########################################################################
# section takes longer then 30 minutes
##########################################################################
def get_step_too_long():

    sql_active = "select userid, system_serial, route_name, step_code, date_trunc('minute',in_stamp)::timestamp _in_ from workflow_process where active = True"

    rs = [dict(r) for r in db.query(sql_active) if r['_in_'] +  minutes_1 * step_time_dct[r['step_code']] < current]

    return rs


##########################################################################
# Last step result set with statements containing id and system_serial
##########################################################################
def get_ser_last_step_dct(sql_stmt):

    rs = [dict(x) for x in db.query(sql_stmt)]

    sys_ser_lst = list(set([k['system_serial'] for k in rs]))      # List of system serials from resulset
    ser_id_lst_dct = {ser : [k['id'] for k in rs if k['system_serial'] == ser ] for ser in  sys_ser_lst}

    # maximum id for a ser associated with last step for this system
    #
    ser_max_id_dct = [ max(ser_id_lst_dct[ser]) for ser in ser_id_lst_dct if ser_id_lst_dct[ser]]

    ser_last_step_dct = {k['system_serial'] : k for k in rs if k['id'] in ser_max_id_dct}

    return ser_last_step_dct


##########################################################################
# System sits idle for longer the 30 minutes. Idle means active = False
# System with issue selected,
# System issue not resolved till 6PM
# System issue unresolver 24 hours
# System burns more than 30 hours
##########################################################################
def get_sys_ser_info_dct():

    sql_today = "select id, system_serial, route_name, step_code, active, issue_code, " + \
                "date_trunc('minute',in_stamp)::timestamp _in_, date_trunc('minute',out_stamp)::timestamp _out_ " + \
                "from workflow_process where date_trunc('day', in_stamp)::timestamp = timestamp '%s' and  active = False" % today_date

    sql_burn  = "select id, system_serial, route_name, step_code, active, issue_code, " + \
                "date_trunc('minute',in_stamp)::timestamp _in_, date_trunc('minute',out_stamp)::timestamp _out_ " + \
                "from workflow_process where date_trunc('hour', out_stamp)::timestamp < timestamp '%s' " % prior_30hrs + \
                "and  active = False and step_code = '090' and issue_code = '999'"

    ser_last_step_dct = get_ser_last_step_dct(sql_today)
    ser_idle_30_dct = {k : ser_last_step_dct[k] for k in ser_last_step_dct if ser_last_step_dct[k]['_out_'] + minutes_30 < current and ser_last_step_dct[k]['step_code'] != '090'}
    ser_with_issue_d = {k : ser_last_step_dct[k] for k in ser_last_step_dct if ser_last_step_dct[k]['issue_code'] != '000'}
    ser_with_issue_dct = {k : ser_with_issue_d[k] for k in ser_with_issue_d if ser_with_issue_d[k]['issue_code'] != '999' or ser_with_issue_d[k]['step_code'] != '090'}

    ser_issue_by_6PM_dct = {}
    if current > cdt_1759:   # Unresolved issues by 6 PM
        ser_issue_by_6PM_dct = dict(ser_with_issue_dct)   # Looks like we don't need it because we have ser_with_issue_dct

    ser_issue_24hrs_dct = {k : ser_last_step_dct[k] for k in ser_last_step_dct if ser_last_step_dct[k]['_out_'] + minutes_1440 < current}

    ser_burn_30hrs_dct = get_ser_last_step_dct(sql_burn)
    
    return  ser_idle_30_dct, ser_with_issue_dct, ser_issue_by_6PM_dct, ser_issue_24hrs_dct, ser_burn_30hrs_dct



##########################################################################
# Each notification has a header
##########################################################################
def rpt_header(title):
    str1 = '%%%%%%%%%%' + title.center(60) + '%%%%%%%%%%'
    rpt_str('\n' + '='*80)
    rpt_str(str1)
    rpt_str('='*80 + '\n')


##########################################################################
# Footer
##########################################################################
def rpt_footer():
    rpt_str('\n' + '^'*80 + '\n')


##########################################################################
# Just names
##########################################################################
def rpt_names_only(title, userids):

    if not userids: return False

    rpt_header(title)
    for nm in userids:
      lst = emp_dct[nm]
      rpt_str('    %s %s' % (lst[0], lst[1]))

    rpt_footer()

    return True


##########################################################################
# 
##########################################################################
def rpt_indirect_activity():

    if not indirect_dct: return False

    rpt_header('Indirect Emloyee Activity')
    rpt_str('-'*58)
    rpt_str('   ' +'User'.ljust(14) + '|  ' + 'From'.ljust(12) + '|  ' + 'Till'.ljust(12) + '|  ' + 'Elapsed'.ljust(10))
    rpt_str('-'*58)
    for k in sorted(indirect_dct):
        for s in indirect_dct[k]:
            # if s[0] != s[1]:
            str2 = '   ' + k.ljust(14) + '|  ' + str(s[0]).split(' ')[-1][:-3].ljust(12) + '|  ' + str(s[1]).split(' ')[-1][:-3].ljust(12) + '|  ' + str(s[1] - s[0])[:-3]
            rpt_str(str2)

    rpt_footer()

    return True




##########################################################################
# step_too_long
##########################################################################
def rpt_step_too_long(title, dct_lst):

    if not dct_lst: return False

    rpt_header(title)
    for k in dct_lst:
        str2 = 'User: %s, System: %s, %s, Step: %s, Started at %s' % (k['userid'], k['system_serial'], k['route_name'], step_dct[k['step_code']], str(k['_in_']))
        rpt_str(str2)

    rpt_footer()

    return True


##########################################################################
# System is idle longer than allowed: ser_idle_30_dct
##########################################################################
def rpt_ser_idle_30(title, dct):

    if not dct: return False

    rpt_header(title)
    for m in sorted(dct):
        k = dct[m]
        str2 = 'System: %s, %s, Finished at %s. Issue: %s' % (k['system_serial'], k['route_name'], str(k['_out_']), issue_c_d[k['issue_code']])
        rpt_str(str2)

    rpt_footer()

    return True


##########################################################################
# System step finished with an issue: ser_with_issue_dct
##########################################################################
def rpt_ser_with_issue(title, dct):

    if not dct: return False

    rpt_header(title)
    for m in sorted(dct):
        k = dct[m]
        str2 = 'System: %s, %s, Finished at %s. Step: %s, Issue: %s' % (k['system_serial'], k['route_name'], str(k['_out_']), step_dct[k['step_code']], issue_c_d[k['issue_code']])
        rpt_str(str2)

    rpt_footer()

    return True


##########################################################################
# Section burning has gone for 30 hours: ser_burn_30hrs
##########################################################################
def rpt_burn_30hrs(title, dct):

    if not dct: return False

    rpt_header(title)
    for m in sorted(dct):
        k = dct[m]
        str2 = 'System: %s, %s, Burn started at %s.' % (k['system_serial'], k['route_name'], str(k['_out_']))
        rpt_str(str2)

    rpt_footer()

    return True


##########################################################################
# Extract all info and generate report
##########################################################################
def report():
    global login_emp_dct, break_emp_dct, indirect_dct

    login_emp_dct = get_today_logins()
    break_emp_dct = get_today_breaks()
    indirect_dct  = get_today_indirect()

    login_after_905 = get_not_login_by_905()
    brk_after_1030  = get_brk_after_1030()
    brk1_longer_than_20 = get_brk1_longer_than_20()
    no_brk_over_30_before_2pm = get_no_brk_over_30_before_2pm()
    brk_after_330  = get_brk2_after_1530()
    brk2_longer_than_20 = get_brk2_longer_than_20()
    less_than_8_hours_login = get_less_than_8_hours_login()

    idle_15_min = get_idle_15_min()
    
    step_too_long = get_step_too_long()
    ser_idle_30_dct, ser_with_issue_dct, ser_issue_by_6pm, ser_issue_24hrs, ser_burn_30hrs = get_sys_ser_info_dct()

    rpt_bool = False
    rpt_bool |= rpt_names_only('Not login in by 9:05', login_after_905)
    rpt_bool |= rpt_names_only('Not take a break by 10:30', brk_after_1030)
    rpt_bool |= rpt_names_only('First break longer than 20 min', brk1_longer_than_20)
    rpt_bool |= rpt_names_only('No break greater then 30 minutes by 2PM', no_brk_over_30_before_2pm)
    rpt_bool |= rpt_names_only('Not take second break by 3:30 PM', brk_after_330)
    rpt_bool |= rpt_names_only('Second break longer than 20 min', brk2_longer_than_20)
    rpt_bool |= rpt_names_only('Logs out with less than 8 hours', less_than_8_hours_login)
    rpt_bool |= rpt_names_only('Idle for more than 15 minutes', idle_15_min)
    rpt_bool |= rpt_indirect_activity()

    rpt_bool |= rpt_step_too_long('Section is longer than expected', step_too_long)
    rpt_bool |= rpt_ser_idle_30('System is idle longer than allowed', ser_idle_30_dct)
    rpt_bool |= rpt_ser_with_issue('System step finished with an issue', ser_with_issue_dct)
    rpt_bool |= rpt_ser_with_issue('Unresolved issues by 6 PM', ser_issue_by_6pm)
    rpt_bool |= rpt_ser_with_issue('Unresolved issues after 24 hours', ser_issue_24hrs)
    rpt_bool |= rpt_burn_30hrs('Section burning has gone for 30 hours', ser_burn_30hrs)
    rptf.close()

    delta_qualifier = True
    if rpt_bool:
        today_dir = get_by_cmd('find %s -name %s' % (static_dir, today_date))
        if not today_dir:
            get_by_cmd('mkdir %s' % static_today)
            get_by_cmd('touch %s%s' % (static_today, today_date))   # To make sure that it is always more than 1 file in there

        get_by_cmd('ln -s %srpts/%s %s%s' % (home_dir, rpt_basename, static_today, rpt_basename))
        rpt_delta = get_by_cmd(diff_last_2_rpt)
        delta_qualifier = rpt_delta != ''

    return rpt_bool and delta_qualifier


##########################################################################
# Close files, connections, and send emails
##########################################################################
def close_all():
    ssh_prod1.close()
    remove_extra_files(home_dir + 'rpts/', 100)
    remove_extra_files(home_dir + 'logs/', 100)
    get_by_cmd('touch %slogo.png' % static_dir)
    remove_extra_files(static_dir, 101)   # should be changed to directories
    logf.close()


##########################################################################
#  main
##########################################################################
def main():
    global logf, rptf, ssh_prod1, login_emp_dct, break_emp_dct

    logf = open(logfilename, 'w')
    ssh_prod1 = get_ssh_con(host_prod1)
    rptf = open(rptfile, 'w')

    rpt_bool = report()
    # rptf.close()
    if rpt_bool:
        link1 = 'http://asherman.alsher.com:9101/static/' + rpt_basename
        web.sendmail('asherman@alsher.com', to_me, 'Report on %s' % datetimetag, 'Report date-time %s.\n\n%s' % (datetimetag,link1), attachments=[rptfile])
    else:
        get_by_cmd('rm -f %srpts/%s %s%s' % (home_dir, rpt_basename, static_today, rpt_basename))

    close_all()

if __name__ == '__main__':
    main()

