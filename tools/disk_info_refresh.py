#!/usr/local/bin/python

import paramiko
k = paramiko.RSAKey.from_private_key_file("/usr/home/asherman/.ssh/id_rsa")
c = paramiko.SSHClient()
c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
c.connect( hostname = "alexs.alsher.com", username = "asherman", pkey = k )
cmd1 = "alexs/alexs/dsk_inf/update_dsk_inf_tbl.sh"
stdin, stdout, stderr = c.exec_command(cmd1)
print stdout.read()
c.close()
