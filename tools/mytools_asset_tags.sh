#!/bin/sh
#
# Usage: ./mytools_asset_tags.sh A1-48971 A1-48972 A1-48973 A1-48974
#
# March, 2017
#
# Alex Sherman
# Software Architect
#
##################################################################################

q=`python -c "print str('$*'.split()).strip('[]')"`
for s in "$@"
do
  echo $s | tr '\n' ' '; psql -h tools.alsher.com -d mytools -U std2 -c "select dmidecode from mv_sys_dmidecode where serial='$s'" | grep 'Chassis Information' -A9 | grep 'Asset Tag' | sed 's/To Be Filled By.*/Nothing/g' | tr -d ' ' | cut -d ':' -f2 | sed 's/^$/Nothing/g';
done | tr -d '+'

