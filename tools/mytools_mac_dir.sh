#!/bin/sh
#
# Usage: ./mytools_mac_dir.sh A1-48971 A1-48972 A1-48973 A1-48974
#
# March, 2017
#
# Alex Sherman
# Software Architect
#
##################################################################################

q=`python -c "print str('$*'.split()).strip('[]')"`
if [ ! -z "$q" ]; then
   psql -h tools.alsher.com -d mytools -U std2 -c "select serial, dir from mv_sys_dir_last where serial in ($q)"
fi
