#!/bin/sh
#
# Usage ./mytools_order_parts.sh A1-53666 A1-53668 A1-53680
#
# March 2017
#
# Example: ./get_sys_parts_ext_1.sh A1-48971
#
# Alex Sherman
# Software Architect
#
#################################################################################

r=`psql -h tools.alsher.com -U std2 -d mytools -c "select system_serial from v_order_cnfg_sys where order_number = '$1'" | grep A1- | sort | tr '\n' ' ' | sed 's/ $//g'`

q1="select order_number, config_name, system_serial from v_order_cnfg_sys where system_serial = '"
q2="select part, model, serial, rma, revision, support_number from v_system_parts where system_serial = '"
q2_end="' order by system_serial, part, model, serial;"
q3="select mac_name,mac from v_system_mac where system_serial ='"
f=""

a="/tmp/sys_parts_$$_"`date "+%H%M%S"`".txt"
echo "" > $a

for s in $r
do
   echo "\x" >> $a
   echo $q1$s"';" >> $a
   echo "\x" >> $a
   echo "\t" >> $a
   echo $q3$s"';" >> $a
   echo "\t" >> $a
   echo $q2$s$q2_end >> $a
done

psql -h tools.alsher.com -U std2 -d mytools -f $a | grep -v -i -e RECORD -e 'Expanded display' -e row -e Tuples
rm -f $a

