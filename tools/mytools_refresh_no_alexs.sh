#!/bin/sh
#
# To refresh materialized views in databases.
# Database name is a parameter
# Materialized views taking longest time are executed in separate threads.
#
# Feb. 2017
# Alex Sherman
# Software Architect
# 
################################################################################################

echo '1. Refresh all MATERIALIZED VIEWs in mytools database'
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW production_part" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_order_ct_cfg_sys_all" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_sys_parts_ext" &

/usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 << EOF
REFRESH MATERIALIZED VIEW production_configuration;
REFRESH MATERIALIZED VIEW production_mac;
REFRESH MATERIALIZED VIEW production_manufacturer;
REFRESH MATERIALIZED VIEW production_order;
REFRESH MATERIALIZED VIEW production_system;
REFRESH MATERIALIZED VIEW production_type;
REFRESH MATERIALIZED VIEW shipping_carrier;
REFRESH MATERIALIZED VIEW shipping_carrier_methods;
REFRESH MATERIALIZED VIEW shipping_method;
REFRESH MATERIALIZED VIEW shipping_tracking;
REFRESH MATERIALIZED VIEW shipping_trackingitem;
EOF

echo '2. DONE!'

