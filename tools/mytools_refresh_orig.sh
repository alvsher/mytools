#!/bin/sh
#
# To refresh materialized views in databases.
# Database name is a parameter
# Materialized views taking longest time are executed in separate threads.
#
# Feb. 2017
# Alex Sherman
# Software Architect
################################################################################################

echo '1. Refresh all MATERIALIZED VIEWs in mytools database'
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW production_part" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_sys_sysinfo" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_sys_ifconfig" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_sys_dmidecode" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_sys_dmesg" &

nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_runid_last" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_runid_last_end" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_sys_dir_last" &
nohup /usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -c "REFRESH MATERIALIZED VIEW mv_sys_dir_last_end" &

nohup /home/asherman/projects/mytools/mytools_tools/disk_info_refresh.py &

/usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 << EOF
REFRESH MATERIALIZED VIEW production_configuration;
REFRESH MATERIALIZED VIEW production_mac;
REFRESH MATERIALIZED VIEW production_manufacturer;
REFRESH MATERIALIZED VIEW production_order;
REFRESH MATERIALIZED VIEW production_system;
REFRESH MATERIALIZED VIEW production_type;
REFRESH MATERIALIZED VIEW shipping_carrier;
REFRESH MATERIALIZED VIEW shipping_carrier_methods;
REFRESH MATERIALIZED VIEW shipping_method;
REFRESH MATERIALIZED VIEW shipping_tracking;
REFRESH MATERIALIZED VIEW shipping_trackingitem;
EOF

echo '2 BACKUP Table sys_disk_info'
/usr/local/bin/pg_dump -h tools.alsher.com -d mytools -U std2 -t sys_disk_info > /home/asherman/projects/mytools/mytools_tools/sys_disk_info.sql

echo '3. DONE!'

