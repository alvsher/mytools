#!/bin/sh
#
# Usage: ./mytools_sys_mem_size.sh A1-53666  A1-53668 A1-53680
#
# March 2017
# Alex Sherman
# Software Architect
# 
################################################################################################

q=`python -c "print str('$*'.split()).strip('[]')"`

a=`psql -h tools.alsher.com -d mytools -U std2 -c "select serial, dmidecode from mv_sys_dmidecode where serial in ($q)" | grep -e "^ A1-" -e 'Memory Array Mapped' -A4 | grep -e "^ A1-" -e 'Range ' | sed -e 's/ |.*dmidecode.*//g' -e 's/.*://g' | awk '{print $1}' | tr '\n' ' ' | sed -e 's/ $//g' -e 's/ / GB@/g' | tr '@' '\n'`
echo $a
# echo " System  RAM"
# echo "------------"
# python -c "for s in ['A1'+k for k in '$a'.strip().split('A1') if k]: print s.split()[0], sum(int(l) for l in s.split()[1:])"
