#!/bin/sh
#
# Usage ./mytools_users.create.sh
#
# March 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################

/usr/local/bin/psql -h tools.alsher.com -d mytools -U std2 -f /home/asherman/projects/mytools/mytools_tools/mytools_users_create.sql
/home/asherman/projects/mytools/mytools_tools/mytools_users_pswd.py
