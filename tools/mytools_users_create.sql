-- March 2017
--
-- Example: psql -h tools.alsher.com -d mytools -U std2 -f mytools_schema.sql
--
-- Alex Sherman
------------------------------------------------------------------------------------

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = true;

--
-- Name: ; Type: TABLE; Schema: public; Owner: std2; Tablespace: 
--

DROP TABLE IF EXISTS mytools_user;

CREATE TABLE mytools_user(
    id                 integer NOT NULL,
    userid             varchar(20) NOT NULL,
    fname              varchar(30) NOT NULL,
    lname              varchar(30) NOT NULL,
    password           varchar(40) NOT NULL,
    email              varchar(40) NOT NULL,
    admin              boolean DEFAULT False,
    PRIMARY KEY (id)
);

ALTER TABLE public.mytools_user OWNER TO std2;

CREATE SEQUENCE mytools_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.mytools_user_id_seq OWNER TO std2;
ALTER SEQUENCE mytools_user_id_seq OWNED BY mytools_user.id;
ALTER TABLE ONLY mytools_user ALTER COLUMN id SET DEFAULT nextval('mytools_user_id_seq'::regclass);

CREATE UNIQUE INDEX mytools_user_uid ON mytools_user (userid);
CREATE UNIQUE INDEX mytools_user_email ON mytools_user (email);
CREATE INDEX mytools_user_fname ON mytools_user (fname);
CREATE INDEX mytools_user_lname ON mytools_user (lname);

DROP TABLE IF EXISTS tmp1;
CREATE TABLE tmp1 AS TABLE mytools_user WITH NO DATA;
ALTER TABLE tmp1 DROP COLUMN id;
ALTER TABLE tmp1 DROP COLUMN password;
\copy tmp1 FROM '/home/asherman/projects/mytools/using/users.txt' CSV;
INSERT INTO mytools_user (userid, fname, lname, password, email, admin) (SELECT userid, fname, lname, '' as password, email, admin FROM tmp1);
DROP TABLE tmp1;
