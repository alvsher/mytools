#!/usr/local/bin/python
#
# March 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################
import sys
import web
import hashlib

if len(sys.argv) < 6:
    print 'Usage: mytools_users_pswd_one.py userid firstname lastname email admin(True/False)'
    exit(0)
else:
    if sys.argv[5] not in ['True','False']:
       print 'Not specified admin rights'
       exit(0)
    elif '@' not in sys.argv[4]:
       print 'Not specified email address'
       exit(0)
    elif len(sys.argv[1]) < 2:
       print 'User ID is too short'
       exit(0)

db=web.database(host='tools.alsher.com', dbn='postgres', db='mytools', user='std2')
db.insert('mytools_user', userid=sys.argv[1], fname=sys.argv[2], lname=sys.argv[3], email = sys.argv[4], password = hashlib.sha1('abcd1234' + sys.argv[4]).hexdigest(), admin = sys.argv[5])

