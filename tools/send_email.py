#!/usr/bin/env /usr/local/bin/python
##################################################################################
#
###################################################################################

import sys
import os
import base64

import time
from datetime import date, timedelta, datetime
from dateutil import parser

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

from email.parser import HeaderParser
import imaplib, email, re

user = base64.b64decode('WVhOb1pYSnRZVzQ9')
passwd = base64.b64decode('VEhWaVlUQXdNREE9')

mailbox = base64.b64decode(user) + '@alsher.com'

the_dir = '/home/asherman/projects/mytools/static/asherman/'


#############################################################
# Execute Unix cmd and return stdout 
#############################################################
def get_by_cmd(cmd):
    ftemp = os.popen(cmd)
    return ftemp.read().rstrip("\n")


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# note that if you want to get text content (body) and the email contains
# multiple payloads (plaintext/ html), you must parse each message separately.
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def get_first_text_block(email_message_instance):
    maintype = email_message_instance.get_content_maintype()
    if maintype == 'multipart':
        for part in email_message_instance.get_payload():
            if part.get_content_maintype() == 'text':
                return part.get_payload()
    elif maintype == 'text':
        return email_message_instance.get_payload()


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Get info from email
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def get_email_info():
    info_dict = {}
    inv_dict = {}
    sys_lst = []
    mail_str = []
    order_num = ''
    invoice = 0
    HOST = 'mail.alsher.com'
    PASSWORD = base64.b64decode('VEhWaVlUQXdNREE9')

    data = []
    for x in range(1,11):
        try:
            server = imaplib.IMAP4_SSL(HOST) # connect
            server.login(mailbox, base64.b64decode(PASSWORD)) # login
            server.select('INBOX',readonly=True) # select mailbox aka folder

            # result, data = server.search(None, "ALL") # search emails
            result, data = server.search(None, '(FROM "std@alsher.com")')
            break
        except:
            print "Not connected to email box yet. Try ", str(x)
            time.sleep(5)
            pass

    if not data:
       return {}, []
    ids = data[0] # data is a list.
    id_list = ids.split() # ids is a space separated string

    for i in range(0,len(id_list)):
       if len(info_dict) > 19:
          break

       inv_dict = {}
       sys_lst = []
       result, data = server.fetch(id_list[-i-1], "(RFC822)") # fetch email
       for item in data:
           if isinstance(item, tuple):
	      msg = email.message_from_string(item[1])
              s = msg['subject'].lower()
              varDate = msg['Date']
              if 'shipping' in s and 'order' in s:
                  # print varDate
	          a = get_first_text_block(email.message_from_string(data[0][1])).replace('>','\n')
                  lines = filter(lambda k: '<' not in k and k != '' and '=' not in k , a.replace('\r','').split('\n'))
                  if 'Systems:' not in str(lines):
                      continue
	          for b in lines:
		      if ':' not in b:
                         # print b.strip()
			 if '  A1-' in b or '  R1-' in b:
                            sys_ser = b.strip().split(' ')[0]
                            sys_lst.append(sys_ser)
		         continue
		      # print "Verify: ", b
		      [c,d] = b.split(':')[:2]
		      if not d: continue   # Line 'Systems:' does not have anything on right hand
	              inv_dict[c.strip()] = d.strip()

	      if not sys_lst:
		 continue
              inv_ord = inv_dict['Invoice'] + '|' + inv_dict['Order']
              info_dict[inv_ord] = [sys_lst, inv_dict]
	      mail_str.append(str((inv_ord, sys_lst, inv_dict)))  # Stringize it
              
    server.close()
    server.logout()
    return info_dict, mail_str

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Send result to recepients
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def send_file_by_email(e_lst):
    msg = MIMEMultipart()
    msg['Subject'] = "MYTOOLS file is attached" 
    msg['From'] = 'asherman@alsher.com'
    msg['To'] = e_lst

    part = MIMEBase('application', "octet-stream")
    part.set_payload(open("/home/asherman/projects/mytools/static/asherman/validation_20170504-164344_14194.txt", "rb").read())
    Encoders.encode_base64(part)

    part.add_header('Content-Disposition', 'attachment; filename="validation_20170504-164344_14194.txt"')

    msg.attach(part)

    server = smtplib.SMTP("mail.alsher.com")
    server.sendmail("asherman@alsher.com", e_lst, msg.as_string())


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Send result to recepients
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def send_test_by_email(e_lst):
    msg = MIMEMultipart()
    msg['Subject'] = "File is attached" 
    msg['From'] = 'asherman@alsher.com'
    msg['To'] = ','.join(e_lst)

    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(the_dir + "validation_20170504-164344_14194.txt", "rb").read())
    Encoders.encode_base64(part)

    part.add_header('Content-Disposition', 'attachment; filename="validation_20170504-164344_14194.txt"')

    msg.attach(part)

    server = smtplib.SMTP("mail.alsher.com")
    server.sendmail("asherman@alsher.com", "asherman@alsher.com", msg.as_string())


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Send result to recepients
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def send_file(p_from, p_filepath, p_filename, e_lst):
    msg = MIMEMultipart()
    msg['Subject'] = "MYTOOLS file is attached" 
    msg['From'] = p_from
    msg['To'] = ','.join(e_lst)
    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(p_filepath, "rb").read())
    Encoders.encode_base64(part)

    part.add_header('Content-Disposition', 'attachment; filename="%s"' % p_filename)

    msg.attach(part)

    server = smtplib.SMTP("mail.alsher.com")
    server.sendmail("asherman@alsher.com", e_lst, msg.as_string())


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Send Disk Info For Update 
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def send_msg(p_from, p_to, p_link, str1):
   sender = 'asherman@alsher.com'
   receivers = ['asherman@alsher.com']

   message = "From: MYTOOLS <" + sender + ">\n" + \
             "To: To user <" + receivers[0] + ">\n" + \
             "Subject: !!!! Attachment from MYTOOLS !!!!\n\n" + \
	     "  +++++ The message: '" + p_link.strip() + "'\n"
   try:
      smtpObj = smtplib.SMTP('mail.alsher.com')
      smtpObj.sendmail(sender, receivers, message)
   except:
      print "Error: unable to send email"



##########################################################################
#  main
#  The variable send_list contains emails to whom the results will be sent
##########################################################################
def main():

    m_from = 'asherman@alsher.com'
    m_to = ['asherman@alsher.com']
    m_subject = 'MYTOOLS file attached %s' % 'test'
    m_content = 'date-time %s.\n\n%s' % ('logintime', 'filename')
    # result = get_by_cmd('ls -tr ' + u_dct['user_static_dir'] + ' | tail -1')
    # m_attachment = u_dct['user_static_dir'] + result
    # print 'm_attachment ; ', m_attachment
    # sendmail('asherman@alsher.com', ['asherman@alsher.com'], 'hi', 'test')
    # send_msg(m_from, m_to, 'http://asherman.alsher.com:9200/static/asherman/validation_20170504-164344_14194.txt', 'Just testing')
    send_file(m_from, '/home/asherman/projects/mytools/static/asherman/validation_20170504-164344_14194.txt', 'validation_20170504-164344_14194.txt', m_to)
    # send_file_by_email(m_to)
    # send_test_by_email(m_to)

if __name__ == '__main__':
    main()
