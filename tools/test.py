#!/usr/local/bin/python
#
# March 2017
#
# Alex Sherman
# Software Architect
#
#################################################################################

import web
import hashlib

db=web.database(host='tools.alsher.com', dbn='postgres', db='mytools', user='std2')
rs = [(r['userid'], hashlib.sha1('abcd1234' + r['email']).hexdigest()) for r in db.query('select userid, email from mytools_user')]
map(lambda r: db.update('mytools_user', where="userid ='%s'" % r[0], password = r[1]), rs)
